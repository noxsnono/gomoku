/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nodes.class.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/18 16:24:21 by vjacquie          #+#    #+#             */
/*   Updated: 2015/07/18 18:51:56 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <nodes.class.hpp>

Nodes::Nodes( void ): status(true) {
	this->pos.push_back(0);
	this->pos.push_back(0);
	this->child = new std::list<Nodes *>();
}

Nodes::Nodes( Nodes const & src ) { *this = src; }

Nodes & Nodes::operator=( Nodes const & rhs ) {
	if (this != &rhs) {}
	return (*this);
}

Nodes::~Nodes( void ) {}

int		Nodes::calc_val( void ) {
	return (0);
}

void	Nodes::next( void ) {
	if (this->actuel != this->child->end())
		this->actuel++;
	else
		this->status = false;
}

Nodes 	Nodes::get_child( void ) { return *(*actuel); }
