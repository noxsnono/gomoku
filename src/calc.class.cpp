/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calc.class.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/03 13:28:52 by vjacquie          #+#    #+#             */
/*   Updated: 2015/09/19 15:56:37 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <calc.class.hpp>

Calc::Calc( void ) {}

Calc::Calc( Map *map ) { this->_map = map; }

Calc::Calc( Calc const & src ) { *this = src; }

Calc & Calc::operator=( Calc const & rhs ) {
	if (this != &rhs) {}
	return (*this);
}

Calc::~Calc( void ) {}

void	Calc::init( Event * event, int ac, char **av ) {
	static_cast<void>(ac);
	static_cast<void>(av);
	this->main_event = event;
}

bool	Calc::is_capture_possible(int y, int x, int player) {
	int adv = (player % 2) + 1;

	if (good_val_full(y, x - 2) == true && good_val_full(y, x + 1) == true
		&& (*this->_map)[y][x - 1] == player && (*this->_map)[y][x - 2] == adv
		&& (*this->_map)[y][x + 1] == 0)
		return (true);
	if (good_val_full(y, x - 2) == true && good_val_full(y, x + 1) == true
		&& (*this->_map)[y][x - 1] == player && (*this->_map)[y][x - 2] == 0
		&& (*this->_map)[y][x + 1] == adv)
		return (true);
	if (good_val_full(y, x + 2) == true && good_val_full(y, x - 1) == true
		&& (*this->_map)[y][x + 1] == player && (*this->_map)[y][x + 2] == adv
		&& (*this->_map)[y][x - 1] == 0)
		return (true);
	if (good_val_full(y, x + 2) == true && good_val_full(y, x - 1) == true
		&& (*this->_map)[y][x + 1] == player && (*this->_map)[y][x + 2] == 0
		&& (*this->_map)[y][x - 1] == adv)
		return (true);
	if (good_val_full(y - 2, x) == true && good_val_full(y + 1, x) == true
		&& (*this->_map)[y - 1][x] == player && (*this->_map)[y - 2][x] == adv
		&& (*this->_map)[y + 1][x] == 0)
		return (true);
	if (good_val_full(y - 2, x) == true && good_val_full(y + 1, x) == true
		&& (*this->_map)[y - 1][x] == player && (*this->_map)[y - 2][x] == 0
		&& (*this->_map)[y + 1][x] == adv)
		return (true);
	if (good_val_full(y + 2, x) == true && good_val_full(y - 1, x) == true
		&& (*this->_map)[y + 1][x] == player && (*this->_map)[y + 2][x] == adv
		&& (*this->_map)[y - 1][x] == 0)
		return (true);
	if (good_val_full(y + 2, x) == true && good_val_full(y - 1, x) == true
		&& (*this->_map)[y + 1][x] == player && (*this->_map)[y + 2][x] == 0
		&& (*this->_map)[y - 1][x] == adv)
		return (true);
	if (good_val_full(y - 2, x - 2) == true && good_val_full(y + 1, x + 1) == true
		&& (*this->_map)[y - 1][x - 1] == player && (*this->_map)[y - 2][x - 2] == adv
		&& (*this->_map)[y + 1][x + 1] == 0)
		return (true);
	if (good_val_full(y - 2, x - 2) == true && good_val_full(y + 1, x + 1) == true
		&& (*this->_map)[y - 1][x - 1] == player && (*this->_map)[y - 2][x - 2] == 0
		&& (*this->_map)[y + 1][x + 1] == adv)
		return (true);
	if (good_val_full(y + 2, x + 2) == true && good_val_full(y - 1, x - 1) == true
		&& (*this->_map)[y + 1][x + 1] == player && (*this->_map)[y + 2][x + 2] == adv
		&& (*this->_map)[y - 1][x - 1] == 0)
		return (true);
	if (good_val_full(y + 2, x + 2) == true && good_val_full(y - 1, x - 1) == true
		&& (*this->_map)[y + 1][x + 1] == player && (*this->_map)[y + 2][x + 2] == 0
		&& (*this->_map)[y - 1][x - 1] == adv)
		return (true);
	if (good_val_full(y + 2, x - 2) == true && good_val_full(y - 1, x + 1) == true
		&& (*this->_map)[y + 1][x - 1] == player && (*this->_map)[y + 2][x - 2] == adv
		&& (*this->_map)[y - 1][x + 1] == 0)
		return (true);
	if (good_val_full(y + 2, x - 2) == true && good_val_full(y - 1, x + 1) == true
		&& (*this->_map)[y + 1][x - 1] == player && (*this->_map)[y + 2][x - 2] == 0
		&& (*this->_map)[y - 1][x + 1] == adv)
		return (true);
	if (good_val_full(y - 2, x + 2) == true && good_val_full(y + 1, x - 1) == true
		&& (*this->_map)[y - 1][x + 1] == player && (*this->_map)[y - 2][x + 2] == adv
		&& (*this->_map)[y + 1][x - 1] == 0)
		return (true);
	if (good_val_full(y - 2, x + 2) == true && good_val_full(y + 1, x - 1) == true
		&& (*this->_map)[y - 1][x + 1] == player && (*this->_map)[y - 2][x + 2] == 0
		&& (*this->_map)[y + 1][x - 1] == adv)
		return (true);
	return (false);
}

bool	Calc::capture_before_win(int player, int start[], int end[]) {
	int x = 0, y = 0, i = 0, j = 0, count = 0;

	while (count < 5) {
		if (count == 0) {
			this->main_event->line_winner.p1[0] = start[0] + j;
			this->main_event->line_winner.p1[1] = start[1] + i;
		}
		else if (count == 1) {
			this->main_event->line_winner.p2[0] = start[0] + j;
			this->main_event->line_winner.p2[1] = start[1] + i;
		}
		else if (count == 2) {
			this->main_event->line_winner.p3[0] = start[0] + j;
			this->main_event->line_winner.p3[1] = start[1] + i;
		}
		else if (count == 3) {
			this->main_event->line_winner.p4[0] = start[0] + j;
			this->main_event->line_winner.p4[1] = start[1] + i;
		}
		else if (count == 4) {
			this->main_event->line_winner.p5[0] = start[0] + j;
			this->main_event->line_winner.p5[1] = start[1] + i;
		}
		count++;
		if (start[0] < end[0])
			j++;
		else if (start[0] > end[0])
			j--;
		if (start[1] < end[1])
			i++;
		else if (start[1] > end[1])
			i--;
	}
	while (start[0] + y != end[0] || start[1] + x != end[1]) {
		if (is_capture_possible(start[0] + y, start[1] + x, player) == true) {
			ft42::win_align = true;
			ft42::win_i = 0;
			this->main_event->line_winner.player = (*this->_map)[start[0]][start[1]];
			return (true);
		}
		if (start[0] < end[0])
			y++;
		else if (start[0] > end[0])
			y--;
		if (start[1] < end[1])
			x++;
		else if (start[1] > end[1])
			x--;
	}
	if (is_capture_possible(start[0] + y, start[1] + x, player) == true) {
		ft42::win_align = true;
		ft42::win_i = 0;
		this->main_event->line_winner.player = (*this->_map)[start[0]][start[1]];
		return (true);
	}
	return (false);
}

bool	Calc::check_win_ver(int player, std::vector<int> pos) {
	int nbr = 0,
	i = 0,
	j = -5;
	int start[2] = {0};
	int end[2] = {0};

	while (j < 5 && nbr < 5) {
		if (good_val(pos[0] + j) && good_val(pos[1] + i)) {
			if ((*this->_map)[pos[0] + j][pos[1] + i] == player)
				nbr++;
			else
				nbr = 0;
		}
		j++;
	}
	start[0] = j - 1 + pos[0];
	start[1] = pos[1];
	end[0] = -4 + start[0];
	end[1] = start[1];
	if (nbr >= 5
		&& true == good_val_full(start[0], start[1])
		&& true == good_val_full(end[0], end[1])
		&& capture_before_win(player, start, end) == false) {
		return (true);
	}
	return (false);		
}

bool	Calc::check_win_hor(int player, std::vector<int> pos) {
	int nbr = 0,
		i = -5,
		j = 0,
		start[2] = {0},
		end[2] = {0};

	while (i < 5 && nbr < 5) {
		if (good_val(pos[0] + j) && good_val(pos[1] + i)) {
			if ((*this->_map)[pos[0] + j][pos[1] + i] == player)
				nbr++;
			else
				nbr = 0;
		}
		i++;
	}
	start[0] = pos[0];
	start[1] = i - 1 + pos[1];
	end[0] = start[0];
	end[1] = -4 + start[1];
	if (nbr >= 5
		&& true == good_val_full(start[0], start[1])
		&& true == good_val_full(end[0], end[1])
		&& capture_before_win(player, start, end) == false) {
		return (true);
	}
	return (false);		
}

bool	Calc::check_win_diag1(int player, std::vector<int> pos) {
	int nbr = 0,
	i = -5,
	j = -5;
	int start[2] = {0};
	int end[2] = {0};

	while (i < 5 && nbr < 5) {
		if (good_val(pos[0] + j) && good_val(pos[1] + i)) {
			if ((*this->_map)[pos[0] + j][pos[1] + i] == player)
				nbr++;
			else
				nbr = 0;
		}
		i++;
		j++;
	}
	start[0] = j - 1 + pos[0];
	start[1] = i - 1 + pos[1];
	end[0] = -4 + start[0];
	end[1] = -4 + start[1];
	if (nbr >= 5
		&& true == good_val_full(start[0], start[1])
		&& true == good_val_full(end[0], end[1])
		&& capture_before_win(player, start, end) == false) {
		return (true);
	}
	return (false);		
}

bool	Calc::check_win_diag2(int player, std::vector<int> pos) {
	int nbr = 0,
	i = 5,
	j = -5;
	int start[2] = {0};
	int end[2] = {0};

	while (j < 5 && nbr < 5) {
		if (good_val(pos[0] + j) && good_val(pos[1] + i)) {
			if ((*this->_map)[pos[0] + j][pos[1] + i] == player)
				nbr++;
			else
				nbr = 0;
		}
		i--;
		j++;
	}
	start[0] = j - 1 + pos[0];
	start[1] = i + 1 + pos[1];
	end[0] = -4 + start[0];
	end[1] = 4 + start[1];
	if (nbr >= 5
		&& true == good_val_full(start[0], start[1])
		&& true == good_val_full(end[0], end[1])
		&& capture_before_win(player, start, end) == false) {
		return (true);
	}
	return (false);		
}

bool	Calc::check_win( int player, std::vector<int> pos) { // use pos to check aligment
	if (check_win_hor(player, pos) == true
		|| check_win_ver(player, pos) == true
		|| check_win_diag1(player, pos) == true
		|| check_win_diag2(player, pos) == true)
		return (true);
	return false;
}

bool	Calc::good_val(int x) {
	if (x >= 0 && x < 19)
		return (true);
	else
		return (false);
}

bool	Calc::good_val_full(int x, int y) {
	if (x >= 0 && x < 19 && y >= 0 && y < 19)
		return (true);
	else
		return (false);
}


void	Calc::create_new_align( int start[], int end[], int player ) { // check if pos are in map !!!!!!!
	int tmpStart[2] = {0};
	int size = 0, j = 0, i = 0;

	t_line * elem = NULL;
	if (start[0] < end[0])
		j++;
	else if (start[0] > end[0])
		j--;
	if (start[1] < end[1])
		i++;
	else if (start[1] > end[1])
		i--;
	while (start[0] + j != end[0] || start[1] + i != end[1]) {
		if (good_val_full(start[1] + i, start[0] + j) == true
			&& (*this->_map)[start[0] + j][start[1] + i] == player) {
			if (size == 0) {
				tmpStart[0] = start[0] + j;
				tmpStart[1] = start[1] + i;
			}
		}
		else if (good_val_full(start[1] + i, start[0] + j) == true
			&& (*this->_map)[start[0] + j][start[1] + i] != player && size >= 2) {
			elem = this->main_event->ia->t_lineAlloc();
			elem->pStart[0] = tmpStart[0];
			elem->pStart[1] = tmpStart[1];
			elem->pStart[0] = (start[0] < end[0]) ? (elem->pStart[0] - 1) : (elem->pStart[0] + 1);
			elem->pStart[1] = (start[1] < end[1]) ? (elem->pStart[1] - 1) : (elem->pStart[1] + 1);

			elem->pEnd[0] = start[0] + j;
			elem->pEnd[1] = start[1] + i;			

			if (good_val_full(elem->pStart[1], elem->pStart[0]) == false
				|| (*this->_map)[elem->pStart[0]][elem->pStart[1]] != 0)
				elem->pStart_empty = false;
			if (good_val_full(elem->pEnd[1], elem->pEnd[0]) == false
				|| (*this->_map)[elem->pEnd[0]][elem->pEnd[1]] != 0)
				elem->pEnd_empty = false;
			elem->size = size;
			if (elem->pStart_empty == false && elem->pEnd_empty == false) {
				std::free(elem);
				elem = NULL;
			}
			else {
				this->main_event->ia->line_list.push_back(elem);
			}
			
			size = 0;
		}
		else {
			if (elem != NULL) {
				std::free(elem);
				elem = NULL;
			}
			size = 0;
		}
		if (start[0] + j < end[0])
			j++;
		else if (start[0] + j > end[0])
			j--;
		if (start[1] + i< end[1])
			i++;
		else if (start[1] + i > end[1])
			i--;
	}
}

void	Calc::clean_list_after_capture( void ) {
	std::list<t_line *>::iterator 	it = this->main_event->ia->line_list.begin();
	std::list<t_line *>::iterator 	end = this->main_event->ia->line_list.end();
	int 							x, y, j, i, size, state, tmp;

	while (it != end) {
		if (true == good_val_full((*it)->pStart[0], (*it)->pStart[1])
			&& true == good_val_full((*it)->pEnd[0], (*it)->pEnd[1])) {
			if ((*it)->type == 0) {
				state = true;
				size = 0;
				j = 0;
				i = 0;
				tmp = 0;
				y = (*it)->pEnd[0];
				x = (*it)->pEnd[1];
				if ((*it)->pStart[0] < y)
					j++;
				else if ((*it)->pStart[0] > y)
					j--;
				if ((*it)->pStart[1] < x)
					i++;
				else if ((*it)->pStart[1] > x)
					i--;
				while (state == true && ((*it)->pStart[0] + j != y || (*it)->pStart[1] + i != x)) {
					if (false == good_val_full( (*it)->pStart[0] + j, (*it)->pStart[1] + i ))
						state = false;
					else if ( ( (*it)->pStart[0] + j != y || (*it)->pStart[1] + i != x )
							&& ((*this->_map)[(*it)->pStart[0] + j][(*it)->pStart[1] + i] != (*it)->player
							&& (*this->_map)[(*it)->pStart[0] + j][(*it)->pStart[1] + i] != 0))
						state = false;
					if ( true == good_val_full( (*it)->pStart[0] + j, (*it)->pStart[1] + i )
						&& (*this->_map)[(*it)->pStart[0] + j][(*it)->pStart[1] + i] == 0) {
						if (tmp < size)
							tmp = size;
						size = 0;
					}
					else if (true == good_val_full( (*it)->pStart[0] + j, (*it)->pStart[1] + i )) {
						size++;
					}

					if ((*it)->pStart[0] < y)
						j++;
					else if ((*it)->pStart[0] > y)
						j--;
					if ((*it)->pStart[1] < x)
						i++;
					else if ((*it)->pStart[1] > x)
						i--;
				}
				size = (tmp > size) ? tmp : size;
				if (size != (*it)->size) {
					if ((*it)->size < 3) { // opti - delete sans recommencer
						this->main_event->ia->line_list.erase(it);
						it = this->main_event->ia->line_list.begin();
						end = this->main_event->ia->line_list.end();
					}
					else if ((*it)->size == 3 && size < 2) {
						(*it)->type = 1;
						(*it)->middle_pos[0] = (*it)->pStart[0];
						(*it)->middle_pos[1] = (*it)->pStart[1];
						if ((*it)->pStart[0] > (*it)->pEnd[0])
							(*it)->middle_pos[0] -= 2;
						else if ((*it)->pStart[0] < (*it)->pEnd[0])
							(*it)->middle_pos[0] += 2;
						if ((*it)->pStart[1] > (*it)->pEnd[1])
							(*it)->middle_pos[1] -= 2;
						else if ((*it)->pStart[1] < (*it)->pEnd[1])
							(*it)->middle_pos[1] += 2;
					}
					else if ((*it)->size <= 5 || ((*it)->size == 3 && size >= 2)) { // opti - delete sans recommencer
						create_new_align((*it)->pStart, (*it)->pEnd, (*it)->player);
						this->main_event->ia->line_list.erase(it);
						it = this->main_event->ia->line_list.begin();
						end = this->main_event->ia->line_list.end();
					}
				}
			}
			else {
				if ((*this->_map)[(*it)->pStart[0]][(*it)->pStart[1]] == 0
					|| (*this->_map)[(*it)->pEnd[0]][(*it)->pEnd[1]] == 0) {
					this->main_event->ia->line_list.erase(it);
					it = this->main_event->ia->line_list.begin();
					end = this->main_event->ia->line_list.end();
				}
			}
		}
		if (it != end)
			it++;
	}
}

void	Calc::refresh_blocked_align(int y, int x) {
	std::list<t_line *>::iterator 	it = this->main_event->ia->line_list.begin();
	std::list<t_line *>::iterator 	end = this->main_event->ia->line_list.end();

	while (it != end) {
		if ((*it)->pStart[0] == y && (*it)->pStart[1] == x)
			(*it)->pStart_empty = true;
		if ((*it)->pEnd[0] == y && (*it)->pEnd[1] == x)
			(*it)->pEnd_empty = true;
		it++;
	}
}

int		Calc::modif_map(std::vector<int> pos, int y, int x, int player) {
	int i = 0,
	j = 0,
	nbr = 0;
	(void)player;
	if (pos[0] < y)
		j++;
	else if (pos[0] > y)
		j--;
	if (pos[1] < x)
		i++;
	else if (pos[1] > x)
		i--;
	while (pos[0] + j != y || pos[1] + i != x) {
		(*this->_map)[pos[0] + j][pos[1] + i] = 0;
		refresh_blocked_align(pos[0] + j, pos[1] + i);
		nbr++;
		if (pos[0] < y)
			j++;
		else if (pos[0] > y)
			j--;
		if (pos[1] < x)
			i++;
		else if (pos[1] > x)
			i--;
	}
	clean_list_after_capture();
	return (nbr);
}

int		Calc::check_line(std::vector<int> pos, int y, int x, int player) {
	int i = 0,
	j = 0;
	bool state = true; // true if capture

	if (good_val(pos[0]) != true || good_val(x) != true
		|| good_val(pos[1]) != true || good_val(y) != true )
		return (0);

	if (pos[0] < y)
		j++;
	else if (pos[0] > y)
		j--;
	if (pos[1] < x)
		i++;
	else if (pos[1] > x)
		i--;

	while (state == true && (pos[0] + j != y || pos[1] + i != x)) {
		if ((pos[0] + j != y || pos[1] + i != x)
			&& ((*this->_map)[pos[0] + j][pos[1] + i] == player
			|| (*this->_map)[pos[0] + j][pos[1] + i] == 0))
			state = false;

		if (pos[0] < y)
			j++;
		else if (pos[0] > y)
			j--;
		if (pos[1] < x)
			i++;
		else if (pos[1] > x)
			i--;
	}
	if ((pos[0] + j == y && pos[1] + i == x)
			&& (*this->_map)[pos[0] + j][pos[1] + i] != player)
			state = false;
	if (state == true)
		return (modif_map(pos, y, x, player));
	else
		return (0);
}

int		Calc::check_capture( int player, std::vector<int> pos ) {
	int capture = 0;

	if (good_val(pos[0] - 3) == true) // y - 3
		capture += check_line(pos, pos[0] - 3, pos[1], player);

	if (good_val(pos[0] - 3) == true && good_val(pos[1] - 3) == true) // y - 3, x - 3
		capture += check_line(pos, pos[0] - 3, pos[1] - 3, player);

	if (good_val(pos[1] - 3) == true) // x - 3
		capture += check_line(pos, pos[0], pos[1] - 3, player);

	if (good_val(pos[1] - 3) == true && good_val(pos[0] + 3) == true) // x - 3, y + 3
		capture += check_line(pos, pos[0] + 3, pos[1] - 3, player);

	if (good_val(pos[0] + 3) == true) // y + 3
		capture += check_line(pos, pos[0] + 3, pos[1], player);

	if (good_val(pos[0] + 3) == true && good_val(pos[1] + 3) == true) // y + 3, x + 3()
		capture += check_line(pos, pos[0] + 3, pos[1] + 3, player);

	if (good_val(pos[1] + 3) == true) // x + 3 
		capture += check_line(pos, pos[0], pos[1] + 3, player);

	if (good_val(pos[0] - 3) == true && good_val(pos[1] + 3) == true) // x + 3, y - 3
		capture += check_line(pos, pos[0] - 3, pos[1] + 3, player);
	return (capture);
}

bool	Calc::check_free_three_hor(std::vector<int> pos, int player) {
	int i = -3;

	while (i <= 3) {
		if (good_val(pos[1] + i) == true && good_val(pos[1] + i + 4) == true
			&& (*this->_map)[pos[0]][pos[1] + i] == 0
			&& (*this->_map)[pos[0]][pos[1] + i + 1] == player
			&& (*this->_map)[pos[0]][pos[1] + i + 2] == player
			&& (*this->_map)[pos[0]][pos[1] + i + 3] == player
			&& (*this->_map)[pos[0]][pos[1] + i + 4] == 0)
			return (true);
		i++;
	}

	i = -4;
	while (i <= 4) {
		if (good_val(pos[1] + i) == true && good_val(pos[1] + i + 5) == true
			&& (*this->_map)[pos[0]][pos[1] + i] == 0
			&& (*this->_map)[pos[0]][pos[1] + i + 1] == player
			&& (*this->_map)[pos[0]][pos[1] + i + 2] == 0
			&& (*this->_map)[pos[0]][pos[1] + i + 3] == player
			&& (*this->_map)[pos[0]][pos[1] + i + 4] == player
			&& (*this->_map)[pos[0]][pos[1] + i + 5] == 0)
			return (true);
		i++;
	}

	i = -4;
	while (i <= 4) {
		if (good_val(pos[1] + i) == true && good_val(pos[1] + i + 5) == true
			&& (*this->_map)[pos[0]][pos[1] + i] == 0
			&& (*this->_map)[pos[0]][pos[1] + i + 1] == player
			&& (*this->_map)[pos[0]][pos[1] + i + 2] == player
			&& (*this->_map)[pos[0]][pos[1] + i + 3] == 0
			&& (*this->_map)[pos[0]][pos[1] + i + 4] == player
			&& (*this->_map)[pos[0]][pos[1] + i + 5] == 0)
			return (true);
		i++;
	}

	return (false);
}

bool	Calc::check_free_three_ver(std::vector<int> pos, int player) {
	int j = -3;

	while (j <= 3) {
		if (good_val(pos[0] + j) == true && good_val(pos[0] + j + 4) == true
		&& (*this->_map)[pos[0] + j][pos[1]] == 0
		&& (*this->_map)[pos[0] + j + 1][pos[1]] == player
		&& (*this->_map)[pos[0] + j + 2][pos[1]] == player
		&& (*this->_map)[pos[0] + j + 3][pos[1]] == player
		&& (*this->_map)[pos[0] + j + 4][pos[1]] == 0)
			return (true);
		j++;
	}
	j = -4;
	while (j <= 4) {
		if (good_val(pos[0] + j) == true && good_val(pos[0] + j + 5) == true
		&& (*this->_map)[pos[0] + j][pos[1]] == 0
		&& (*this->_map)[pos[0] + j + 1][pos[1]] == player
		&& (*this->_map)[pos[0] + j + 2][pos[1]] == 0
		&& (*this->_map)[pos[0] + j + 3][pos[1]] == player
		&& (*this->_map)[pos[0] + j + 4][pos[1]] == player
		&& (*this->_map)[pos[0] + j + 5][pos[1]] == 0)
			return (true);
		j++;
	}
	j = -4;
	while (j <= 4) {
		if (good_val(pos[0] + j) == true && good_val(pos[0] + j + 5) == true
		&& (*this->_map)[pos[0] + j][pos[1]] == 0
		&& (*this->_map)[pos[0] + j + 1][pos[1]] == player
		&& (*this->_map)[pos[0] + j + 2][pos[1]] == player
		&& (*this->_map)[pos[0] + j + 3][pos[1]] == 0
		&& (*this->_map)[pos[0] + j + 4][pos[1]] == player
		&& (*this->_map)[pos[0] + j + 5][pos[1]] == 0)
			return (true);
		j++;
	}
	return (false);
}

bool	Calc::check_free_three_diag1(std::vector<int> pos, int player) {
	(void)pos;
	(void)player;
	return (false);
}

bool	Calc::check_free_three_diag2(std::vector<int> pos, int player) {
	(void)pos;
	(void)player;
	return (false);
}

bool	Calc::check_free_three(std::vector<int> pos, int player) {

	int res = 0;

	if (check_free_three_ver(pos, player) == true) {
		res++;
	}
	if (check_free_three_hor(pos, player) == true) {
		res++;
	}
	if (res >= 2)
		return (true);
	return (false);
}

bool	Calc::check_pos(std::vector<int> pos, int player) {
	if (false == good_val_full(pos[0], pos[1]))
		return (false);
	if ((*this->_map)[pos[0]][pos[1]] != 0)
		return (false);
	(*this->_map)[pos[0]][pos[1]] = player;
	if (check_free_three(pos, player) == true) {
		(*this->_map)[pos[0]][pos[1]] = 0;
		return (false);
	}
	(*this->_map)[pos[0]][pos[1]] = 0;
	return (true); // test line
}
