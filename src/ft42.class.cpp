#include <ft42.class.hpp>

std::string			ft42::logFileName;
std::ofstream		ft42::lodFD;
bool				ft42::logg;
bool				ft42::iathink;
int					ft42::cmd_depth;
int 				ft42::node_total;
int 				ft42::score_max;
int					ft42::entry_d;
int					ft42::win_i;
bool				ft42::win_align;
bool				ft42::draw_last_played;
bool				ft42::two_players;
std::list<char *>	ft42::node_score;

char *		ft42::serialize(int x, int y, int score) {
	char * tmp = NULL;

	tmp = (char *)malloc(sizeof(char) * 24);
	if (NULL == tmp) {
		this->w_full("ft42::serialize Malloc error.");
		throw std::exception();
	}	
	sprintf (tmp, "x=%02d y=%02d", x, y);
	(void)score;
	ft42::node_score.push_back(tmp);
	return tmp;
}

Map	*		ft42::copy_map( Map *map ) {
	Map * 	tmp = new Map();
	std::vector<int> *	line = NULL;
	int 	y = 0;

	if (NULL == tmp) {
		this->w_full("ft42::copy_map1 tmp = new() error");
		throw std::exception();
	}
	while (y < SIZE) {
		line = new std::vector<int>((*map)[y]);

		if (line == NULL) {
			this->w_full("ft42::copy_map1 line = new() error");
			throw std::exception();
		}
		tmp->push_back(*line);
		line->clear();
		if (line != NULL)
			delete line;
		y++;
	}
	return (tmp);
}

void			ft42::w_full( std::string const newEntry ) {
	if (true == ft42::logg) {
		std::cout << newEntry << std::endl;
		ft42::lodFD << ft42::logTime( newEntry ) << std::endl;
	}	
}

void			ft42::w_error( std::string const newEntry ) {
	if (true == ft42::logg) {
		std::cerr << newEntry << std::endl;
		this->w_log(newEntry);
	}
}

void			ft42::w_log( std::string const newEntry ) {
	if (true == ft42::logg) {
		if ( ft42::logFileName.empty() && ft42::lodFD.is_open() == 0) {
			std::stringstream	ss;
			ss << LOG_PATH;
			ft42::lodFD.open(ss.str(), std::ofstream::out | std::ofstream::app);
			this->lodFD << std::endl << std::endl << std::endl << std::endl << std::endl;
			if (this->lodFD.fail() == 1) {
				std::cerr << "Log File Open error" << std::endl;
				throw std::exception();
			}
		}
		this->lodFD << ft42::logTime( newEntry ) << std::endl;
	}
}

std::string		ft42::logTime( std::string const & sentence ) {
	std::stringstream	ss;

	memset(&this->buffer, 0, 80);
	time(&this->timer);
	this->timeinfo = localtime (&this->timer);
	strftime (this->buffer, 80, "%Y/%m/%d %H:%M:%S ", this->timeinfo);
	ss << this->buffer;
	this->result = ss.str() + sentence;
	return (this->result);
}

std::ofstream &	ft42::getLogFD( void ) { return ( ft42::lodFD ); }

ft42::ft42() {}

ft42::~ft42() {}
