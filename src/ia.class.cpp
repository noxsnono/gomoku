/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ia.class.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/18 13:53:13 by vjacquie          #+#    #+#             */
/*   Updated: 2015/09/19 15:22:33 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ia.class.hpp>

Ia::Ia( void ) {}

Ia::Ia( Map *map ) { this->_map = map; }

Ia::Ia( Ia const & src ) { *this = src; }

Ia & Ia::operator=( Ia const & rhs ) {
	if (this != &rhs) {}
	return (*this);
}

Ia::~Ia( void ) {}

int     Ia::pos_compare( std::vector<int> *pos1, int pos2[2] ) {
	if ((*pos1)[0] == pos2[0] && (*pos1)[1] == pos2[1])
		return 1;
	return 0;
}

void    Ia::move_point_chain( t_line * line, std::vector<int> *pos, int axis, int type) {
	int		start[2];
	int		end[2];
	int 	count,
			i,
			j,
			player = (*this->_map)[ (*pos)[0] ][ (*pos)[1] ];

	if (axis == e_HORIZONTAL) { // e_HORIZONTAL
		i = 0;
		count = 0;

		while (i < 4 && this->calc->good_val((*pos)[1] + i) == true
			&& (*this->_map)[ (*pos)[0] ][ (*pos)[1] + i ] == player) {
			count++;
			i++;
		}
		start[0] = (*pos)[0];
		start[1] = (*pos)[1];
		start[1] += i;

		i = 1;
		while (i < 4 && this->calc->good_val((*pos)[1] - i) == true
			&& (*this->_map)[ (*pos)[0] ][ (*pos)[1] - i ] == player) {
			count++;
			i++;
		}

		end[0] = (*pos)[0];
		end[1] = (*pos)[1];
		end[1] -= i;
	}
	else if (axis == e_VERTICAL) { // e_VERTICAL
		j = 0;
		count = 0;
		while (j < 4 && this->calc->good_val((*pos)[0] + j) == true
			&& (*this->_map)[ (*pos)[0] + j ][ (*pos)[1] ] == player) {
			count++;
			j++;
		}
		start[0] = (*pos)[0];
		start[1] = (*pos)[1];
		start[0] += j;

		j = 1;
		while (j < 4 && this->calc->good_val((*pos)[0] - j) == true
			&& (*this->_map)[ (*pos)[0] - j][ (*pos)[1] ] == player) {
			count++;
			j++;
		}

		end[0] = (*pos)[0];
		end[1] = (*pos)[1];
		end[0] -= j;
	}
	else if (axis == e_DIAGUP) { // e_DIAGUP
		j = 0;
		i = 0;
		count = 0;

		while (j < 4 && this->calc->good_val((*pos)[0] - j) == true
			&& this->calc->good_val((*pos)[1] + i) == true
			&& (*this->_map)[ (*pos)[0] - j ][ (*pos)[1] + i ] == player) {
			count++;
			j++;
			i++;
		}
		start[0] = (*pos)[0];
		start[1] = (*pos)[1];
		start[0] -= j;
		start[1] += i;

		i = 1;
		j = 1;
		while (j < 4 && this->calc->good_val((*pos)[0] + j) == true
			&& this->calc->good_val((*pos)[1] - i) == true
			&& (*this->_map)[ (*pos)[0] + j][ (*pos)[1] - i ] == player) {
			count++;
			i++;
			j++;
		}

		end[0] = (*pos)[0];
		end[1] = (*pos)[1];
		end[0] += j;
		end[1] -= i;
	}
	else if (axis == e_DIAGDOWN) { // e_DIAGDOWN
		j = 0;
		i = 0;
		count = 0;

		while (j < 4 && this->calc->good_val((*pos)[0] + j) == true
			&& this->calc->good_val((*pos)[1] + i) == true
			&& (*this->_map)[ (*pos)[0] + j ][ (*pos)[1] + i ] == player) {
			count++;
			j++;
			i++;
		}
		start[0] = (*pos)[0];
		start[1] = (*pos)[1];
		start[0] += j;
		start[1] += i;

		i = 1;
		j = 1;
		while (j < 4 && this->calc->good_val((*pos)[0] - j) == true
			&& this->calc->good_val((*pos)[1] - i) == true
			&& (*this->_map)[ (*pos)[0] - j][ (*pos)[1] - i ] == player) {
			count++;
			i++;
			j++;
		}

		end[0] = (*pos)[0];
		end[1] = (*pos)[1];
		end[0] -= j;
		end[1] -= i;
	}

	if (type == START) {
		line->size = count;
		line->pStart[0] = start[0];
		line->pStart[1] = start[1];
	}
	else if (type == END) {
		line->size = count;
		line->pEnd[0] = end[0];
		line->pEnd[1] = end[1];
	}
}

t_line *	Ia::t_lineAlloc( void ) {
	t_line * tmp = (t_line *)std::malloc(sizeof(t_line));

	if (tmp == NULL) {
		this->w_full("Ia::t_lineAlloc, Malloc error !!!");
		throw std::exception();
	}
	tmp->pStart[0] = -1;
	tmp->pStart[1] = -1;
	tmp->pEnd[0] = -1;
	tmp->pEnd[1] = -1;
	tmp->pStart_empty = true;
	tmp->pEnd_empty = true;
	tmp->size = 0;
	tmp->type = 0;
	tmp->middle_pos[0] = -1;
	tmp->middle_pos[1] = -1;
	return tmp;
}

void	Ia::add_spaced_align(int y, int x, int player, int axis, int dir) {
	t_line	*						lineTmp = t_lineAlloc();

	lineTmp->size = 3;
	lineTmp->type = 1;
	lineTmp->middle_pos[0] = y;
	lineTmp->middle_pos[1] = x;
	lineTmp->player = player;

	if (axis == e_HORIZONTAL && dir == 0) {
		lineTmp->pStart[0] = y;
		lineTmp->pStart[1] = x + 1;
		lineTmp->pEnd[0] = y;
		lineTmp->pEnd[1] = x - 1;
	}
	else if (axis == e_HORIZONTAL && dir == 1) {
		lineTmp->pStart[0] = y;
		lineTmp->pStart[1] = x - 1;
		lineTmp->pEnd[0] = y;
		lineTmp->pEnd[1] = x + 1;
	}
	else if (axis == e_VERTICAL && dir == 0) {
		lineTmp->pStart[0] = y + 1;
		lineTmp->pStart[1] = x;
		lineTmp->pEnd[0] = y - 1;
		lineTmp->pEnd[1] = x;
	}
	else if (axis == e_VERTICAL && dir == 1) {
		lineTmp->pStart[0] = y - 1;
		lineTmp->pStart[1] = x;
		lineTmp->pEnd[0] = y + 1;
		lineTmp->pEnd[1] = x;
	}
	else if (axis == e_DIAGUP && dir == 0) {
		lineTmp->pStart[0] = y + 1;
		lineTmp->pStart[1] = x - 1;
		lineTmp->pEnd[0] = y - 1;
		lineTmp->pEnd[1] = x + 1;
	}
	else if (axis == e_DIAGUP && dir == 1) {
		lineTmp->pStart[0] = y - 1;
		lineTmp->pStart[1] = x + 1;
		lineTmp->pEnd[0] = y + 1;
		lineTmp->pEnd[1] = x - 1;
	}
	else if (axis == e_DIAGDOWN && dir == 0) {
		lineTmp->pStart[0] = y + 1;
		lineTmp->pStart[1] = x + 1;
		lineTmp->pEnd[0] = y - 1;
		lineTmp->pEnd[1] = x - 1;
	}
	else if (axis == e_DIAGDOWN && dir == 1) {
		lineTmp->pStart[0] = y - 1;
		lineTmp->pStart[1] = x - 1;
		lineTmp->pEnd[0] = y + 1;
		lineTmp->pEnd[1] = x + 1;
	}
	this->line_list.push_back(lineTmp);
}

void	Ia::check_spaced_align(std::vector<int> *pos) {
	
	int 							player = (*this->_map)[(*pos)[0]][(*pos)[1]];
	std::list<t_line *>::iterator   it = this->line_list.begin();
	std::list<t_line *>::iterator   end = this->line_list.end();

	while (it != end) {
		if ((*pos)[0] == (*it)->middle_pos[0] && (*pos)[1] == (*it)->middle_pos[1]) {
			if ((*this->_map)[(*pos)[0]][(*pos)[1]] == (*it)->player) {
				std::free(*it);
				this->line_list.erase(it);
			}
			break ;
		}
		it++;
	}

	if (this->calc->good_val_full((*pos)[0], -2 + (*pos)[1]) == true
		&& (*this->_map)[(*pos)[0]][-1 + (*pos)[1]] == 0
		&& (*this->_map)[(*pos)[0]][-2 + (*pos)[1]] == player) { // e_HORIZONTAL
		add_spaced_align((*pos)[0], -1 + (*pos)[1], player, e_HORIZONTAL, 0);
	}
	if (this->calc->good_val_full((*pos)[0], 2 + (*pos)[1]) == true
		&& (*this->_map)[(*pos)[0]][1 + (*pos)[1]] == 0
		&& (*this->_map)[(*pos)[0]][2 + (*pos)[1]] == player) { // e_HORIZONTAL
		add_spaced_align((*pos)[0], 1 + (*pos)[1], player, e_HORIZONTAL, 1);
	}

	if (this->calc->good_val_full(-2 + (*pos)[0], (*pos)[1]) == true
		&& (*this->_map)[-1 + (*pos)[0]][(*pos)[1]] == 0
		&& (*this->_map)[-2 + (*pos)[0]][(*pos)[1]] == player) { // e_VERTICAL
		add_spaced_align(-1 + (*pos)[0], (*pos)[1], player, e_VERTICAL, 0);
	}
	if (this->calc->good_val_full(2 + (*pos)[0], (*pos)[1]) == true
		&& (*this->_map)[1 + (*pos)[0]][(*pos)[1]] == 0
		&& (*this->_map)[2 + (*pos)[0]][(*pos)[1]] == player) { // e_VERTICAL
		add_spaced_align(1 + (*pos)[0], (*pos)[1], player, e_VERTICAL, 1);
	}

	if (this->calc->good_val_full(-2 + (*pos)[0], 2 + (*pos)[1]) == true
		&& (*this->_map)[-1 + (*pos)[0]][1 + (*pos)[1]] == 0
		&& (*this->_map)[-2 + (*pos)[0]][2 + (*pos)[1]] == player) { // e_DIAGUP
		add_spaced_align(-1 + (*pos)[0], 1 + (*pos)[1], player, e_DIAGUP, 0);
	}
	if (this->calc->good_val_full(2 + (*pos)[0], -2 + (*pos)[1]) == true
		&& (*this->_map)[1 + (*pos)[0]][-1 + (*pos)[1]] == 0
		&& (*this->_map)[2 + (*pos)[0]][-2 + (*pos)[1]] == player) { // e_DIAGUP
		add_spaced_align(1 + (*pos)[0], -1 + (*pos)[1], player, e_DIAGUP, 1);
	}

	if (this->calc->good_val_full(-2 + (*pos)[0], -2 + (*pos)[1]) == true
		&& (*this->_map)[-1 + (*pos)[0]][-1 + (*pos)[1]] == 0
		&& (*this->_map)[-2 + (*pos)[0]][-2 + (*pos)[1]] == player) { // e_DIAGDOWN
		add_spaced_align(-1 + (*pos)[0], -1 + (*pos)[1], player, e_DIAGDOWN, 0);
	}
	if (this->calc->good_val_full(2 + (*pos)[0], 2 + (*pos)[1]) == true
		&& (*this->_map)[1 + (*pos)[0]][1 + (*pos)[1]] == 0
		&& (*this->_map)[2 + (*pos)[0]][2 + (*pos)[1]] == player) { // e_DIAGDOWN
		add_spaced_align(1 + (*pos)[0], 1 + (*pos)[1], player, e_DIAGDOWN, 1);
	}
}

void    Ia::checkAlign( std::vector<int> *pos ) {
	std::list<t_line *>::iterator   it = this->line_list.begin();
	std::list<t_line *>::iterator   end = this->line_list.end();

	t_line	*						lineTmp;
	int								tmp[2];

	int                             player = (*this->_map)[ (*pos)[0] ][ (*pos)[1] ],
									// dir = this->check_around( *pos ),
									t_axis[4] = {1, 1, 1, 1},
									i, j, count;

	bool                            action = false;

	while (it != end) {

		if ( pos_compare(pos, (*it)->pStart) == 1 && (*it)->player == player ) {
			this->move_point_chain(*it, pos, (*it)->axis, START);
			t_axis[(*it)->axis] = 0;
			action = true;
		}
		else if ( pos_compare(pos, (*it)->pStart) == 1 && (*it)->player != player )
			(*it)->pStart_empty = false;
		else if ( pos_compare(pos, (*it)->pEnd) == 1 && (*it)->player == player ) {
			this->move_point_chain(*it, pos, (*it)->axis, END);
			action = true;
			t_axis[(*it)->axis] = 0;
		}
		else if ( pos_compare(pos, (*it)->pEnd) == 1 && (*it)->player != player )
			(*it)->pEnd_empty = false;
		it++;
	}

	if (t_axis[0] == 1) { // e_HORIZONTAL

		lineTmp = t_lineAlloc();
		i = 0;
		count = 0;

		lineTmp->player = player;
		lineTmp->axis = e_HORIZONTAL;

		while (i < 4 && this->calc->good_val((*pos)[1] + i) == true
			&& (*this->_map)[ (*pos)[0] ][ (*pos)[1] + i ] == player) {
			count++;
			i++;
		}
		tmp[0] = (*pos)[0];
		tmp[1] = (*pos)[1];
		tmp[1] += i;
		lineTmp->pStart[0] = tmp[0];
		lineTmp->pStart[1] = tmp[1];
		if (this->calc->good_val_full(lineTmp->pStart[0], lineTmp->pStart[1]) != true
			|| (*this->_map)[ lineTmp->pStart[0] ][ lineTmp->pStart[1] ] != 0)
			lineTmp->pStart_empty = false;

		i = 1;
		while (i < 4 && this->calc->good_val((*pos)[1] - i) == true
			&& (*this->_map)[ (*pos)[0] ][ (*pos)[1] - i ] == player) {
			count++;
			i++;
		}

		tmp[0] = (*pos)[0];
		tmp[1] = (*pos)[1];
		tmp[1] -= i;
		lineTmp->pEnd[0] = tmp[0];
		lineTmp->pEnd[1] = tmp[1];

		if (this->calc->good_val_full(lineTmp->pEnd[0], lineTmp->pEnd[1]) != true
			|| (*this->_map)[ lineTmp->pEnd[0] ][ lineTmp->pEnd[1] ] != 0)
			lineTmp->pEnd_empty = false;
		if (count > 1) {
			lineTmp->size = count;
			this->line_list.push_back(lineTmp);
		}
		else {
			if (lineTmp != NULL) {
				std::free(lineTmp);
				lineTmp = NULL;
			}
		}		
	}

	if (t_axis[1] == 1) { // e_VERTICAL
		lineTmp = t_lineAlloc();
		j = 0;
		count = 0;
		lineTmp->player = player;
		lineTmp->axis = e_VERTICAL;

		while (j < 4 && this->calc->good_val((*pos)[0] + j) == true
			&& (*this->_map)[ (*pos)[0] + j ][ (*pos)[1] ] == player) {
			count++;
			j++;
		}
		tmp[0] = (*pos)[0];
		tmp[1] = (*pos)[1];
		tmp[0] += j;
		lineTmp->pStart[0] = tmp[0];
		lineTmp->pStart[1] = tmp[1];

		if (this->calc->good_val_full(lineTmp->pStart[0], lineTmp->pStart[1]) != true
			||(*this->_map)[ lineTmp->pStart[0] ][ lineTmp->pStart[1] ] != 0)
			lineTmp->pStart_empty = false;

		j = 1;
		while (j < 4 && this->calc->good_val((*pos)[0] - j) == true
			&& (*this->_map)[ (*pos)[0] - j][ (*pos)[1] ] == player) {
			count++;
			j++;
		}

		tmp[0] = (*pos)[0];
		tmp[1] = (*pos)[1];
		tmp[0] -= j;
		lineTmp->pEnd[0] = tmp[0];
		lineTmp->pEnd[1] = tmp[1];

		if (this->calc->good_val_full(lineTmp->pEnd[0], lineTmp->pEnd[1]) != true
			|| (*this->_map)[ lineTmp->pEnd[0] ][ lineTmp->pEnd[1] ] != 0)
			lineTmp->pEnd_empty = false;

		if (count > 1) {
			lineTmp->size = count;
			this->line_list.push_back(lineTmp);
		}
		else {
			if (lineTmp != NULL) {
				std::free(lineTmp);
				lineTmp = NULL;
			}
		}
	}


	if (t_axis[2] == 1) { // e_DIAGUP
		lineTmp = t_lineAlloc();
		j = 0;
		i = 0;
		count = 0;
		lineTmp->player = player;
		lineTmp->axis = e_DIAGUP;

		while (j < 4 && this->calc->good_val((*pos)[0] - j) == true
			&& this->calc->good_val((*pos)[1] + i) == true
			&& (*this->_map)[ (*pos)[0] - j ][ (*pos)[1] + i ] == player) {
			count++;
			j++;
			i++;
		}
		tmp[0] = (*pos)[0];
		tmp[1] = (*pos)[1];
		tmp[0] -= j;
		tmp[1] += i;
		lineTmp->pStart[0] = tmp[0];
		lineTmp->pStart[1] = tmp[1];

		if (this->calc->good_val_full(lineTmp->pStart[0], lineTmp->pStart[1]) != true
			|| (*this->_map)[ lineTmp->pStart[0] ][ lineTmp->pStart[1] ] != 0)
			lineTmp->pStart_empty = false;

		i = 1;
		j = 1;
		while (j < 4 && this->calc->good_val((*pos)[0] + j) == true
			&& this->calc->good_val((*pos)[1] - i) == true
			&& (*this->_map)[ (*pos)[0] + j][ (*pos)[1] - i ] == player) {
			count++;
			i++;
			j++;
		}

		tmp[0] = (*pos)[0];
		tmp[1] = (*pos)[1];
		tmp[0] += j;
		tmp[1] -= i;
		lineTmp->pEnd[0] = tmp[0];
		lineTmp->pEnd[1] = tmp[1];

		if (this->calc->good_val_full(lineTmp->pEnd[0], lineTmp->pEnd[1]) != true
			|| (*this->_map)[ lineTmp->pEnd[0] ][ lineTmp->pEnd[1] ] != 0)
			lineTmp->pEnd_empty = false;

		if (count > 1) {
			lineTmp->size = count;
			this->line_list.push_back(lineTmp);
		}
		else  {
			if (lineTmp != NULL) {
				std::free(lineTmp);
				lineTmp = NULL;
			}
		}
	}

	if (t_axis[3] == 1) { // e_DIAGDOWN
		lineTmp = t_lineAlloc();
		j = 0;
		i = 0;
		count = 0;
		lineTmp->player = player;
		lineTmp->axis = e_DIAGUP;

		while (j < 4 && this->calc->good_val((*pos)[0] + j) == true
			&& this->calc->good_val((*pos)[1] + i) == true
			&& (*this->_map)[ (*pos)[0] + j ][ (*pos)[1] + i ] == player) {
			count++;
			j++;
			i++;
		}
		tmp[0] = (*pos)[0];
		tmp[1] = (*pos)[1];
		tmp[0] += j;
		tmp[1] += i;
		lineTmp->pStart[0] = tmp[0];
		lineTmp->pStart[1] = tmp[1];

		if (this->calc->good_val_full(lineTmp->pStart[0], lineTmp->pStart[1]) != true
			|| (*this->_map)[ lineTmp->pStart[0] ][ lineTmp->pStart[1] ] != 0)
			lineTmp->pStart_empty = false;

		i = 1;
		j = 1;
		while (j < 4 && this->calc->good_val((*pos)[0] - j) == true
			&& this->calc->good_val((*pos)[1] - i) == true
			&& (*this->_map)[ (*pos)[0] - j][ (*pos)[1] - i ] == player) {
			count++;
			i++;
			j++;
		}

		tmp[0] = (*pos)[0];
		tmp[1] = (*pos)[1];
		tmp[0] -= j;
		tmp[1] -= i;
		lineTmp->pEnd[0] = tmp[0];
		lineTmp->pEnd[1] = tmp[1];

		if (this->calc->good_val_full(lineTmp->pEnd[0], lineTmp->pEnd[1]) != true
			|| (*this->_map)[ lineTmp->pEnd[0] ][ lineTmp->pEnd[1] ] != 0)
			lineTmp->pEnd_empty = false;

		if (count > 1) {
			lineTmp->size = count;
			this->line_list.push_back(lineTmp);
		}
		else {
			if (lineTmp != NULL) {
				std::free(lineTmp);
				lineTmp = NULL;
			}
		}
	}
	check_spaced_align(pos);
}


void    Ia::init( Event * event, Calc * calc) {
	this->main_event = event;
	this->calc = calc;
	srand(time(NULL));
	this->pos.push_back(-1);
	this->pos.push_back(-1);
	this->pos_off = new std::vector<int>();
	if (this->pos_off == NULL) {
		this->w_full("Malloc Error Ia::init: this->pos_off");
		throw std::exception();
	}
	this->pos_off->push_back(-1);
	this->pos_off->push_back(-1);
	this->pos_def = new std::vector<int>();
	if (this->pos_def == NULL) {
		this->w_full("Malloc Error Ia::init: this->pos_def");
		throw std::exception();
	}
	this->pos_def->push_back(-1);
	this->pos_def->push_back(-1);
	this->pos_adv = new std::vector<int>();
	if (this->pos_adv == NULL) {
		this->w_full("Malloc Error Ia::init: this->pos_adv");
		throw std::exception();
	}
	this->pos_adv->push_back(-1);
	this->pos_adv->push_back(-1);
}

//test fct too
void    Ia::print_tree(t_node *nodes, int depth) {
	std::list<struct s_node *>::iterator    it = nodes->child->begin();
	std::list<struct s_node *>::iterator    end = nodes->child->end();

	while (it != end) {
		this->main_event->print_map((*it)->map);
		print_tree((*it), depth + 1);
		it++;
	}
}

int 	Ia::get_score(int size, int type, int player) {
	if (type == ALIGN) {
		if (size == 5) {
			if (player == HUMAN)
				return (-999999);
			else
				return (999999);
		}
		else if (size == 4) {
			if (player == HUMAN)
				return (950000);
			else
				return (850000);
		}
		else if (size == 3) {
			if (player == HUMAN)
				return (350000);
			else
				return (300000);
		}
		else
			return 100000;
	}
	else if (type == CAPTURE) {
		if (size == 1)
			return (this->main_event->_capture[1] * 150000);
		else
			return (this->main_event->_capture[1] * 125000);
	}
	else
		return 0;
}

int		Ia::eval_capture(std::vector<int> pos, int y, int x, int player) {
	int i = 0,
	j = 0;
	bool state = true; // true if capture

	if (this->calc->good_val_full(pos[0], pos[1]) != true
		|| this->calc->good_val_full(x, y) != true)
		return (0);

	if (pos[0] < y)
		j++;
	else if (pos[0] > y)
		j--;
	if (pos[1] < x)
		i++;
	else if (pos[1] > x)
		i--;

	while (state == true && (pos[0] + j != y || pos[1] + i != x)) {

		if ((pos[0] + j != y || pos[1] + i != x)
			&& ((*this->_map)[pos[0] + j][pos[1] + i] == player
			|| (*this->_map)[pos[0] + j][pos[1] + i] == 0)) {
			state = false;
		}
			

		if (pos[0] < y)
			j++;
		else if (pos[0] > y)
			j--;
		if (pos[1] < x)
			i++;
		else if (pos[1] > x)
			i--;
	}
	if ((pos[0] + j == y && pos[1] + i == x)
			&& (*this->_map)[pos[0] + j][pos[1] + i] != player)
			state = false;
	if (state == true) {
		if ((*this->_map)[pos[0]][pos[1]] != player
			&& (*this->_map)[pos[0] + j][pos[1] + i] != 0)
			return (get_score(1, CAPTURE, player));
		else if ((*this->_map)[pos[0]][pos[1]] != player
			&& (*this->_map)[pos[0] + j][pos[1] + i] != 0)
			return (get_score(1, CAPTURE, player));
		else
			return (get_score(0, CAPTURE, player));
	}
	return (0);
}

int 	Ia::eval_spaced_align(std::vector<int> pos) {
	int player = (*this->_map)[pos[0]][pos[1]];
	int adv = (player % 2) + 1;
	int result = 0;

	if (this->calc->good_val_full(pos[0], -2 + pos[1]) == true
		&& (*this->_map)[pos[0]][-1 + pos[1]] == 0
		&& (*this->_map)[pos[0]][-2 + pos[1]] == player) { // e_HORIZONTAL
		result += 250000;
	}
	if (this->calc->good_val_full(pos[0], 2 + pos[1]) == true
		&& (*this->_map)[pos[0]][1 + pos[1]] == 0
		&& (*this->_map)[pos[0]][2 + pos[1]] == player) { // e_HORIZONTAL
		result += 250000;
	}

	if (this->calc->good_val_full(-2 + pos[0], pos[1]) == true
		&& (*this->_map)[-1 + pos[0]][pos[1]] == 0
		&& (*this->_map)[-2 + pos[0]][pos[1]] == player) { // e_VERTICAL
		result += 250000;
	}
	if (this->calc->good_val_full(2 + pos[0], pos[1]) == true
		&& (*this->_map)[1 + pos[0]][pos[1]] == 0
		&& (*this->_map)[2 + pos[0]][pos[1]] == player) { // e_VERTICAL
		result += 250000;
	}

	if (this->calc->good_val_full(-2 + pos[0], 2 + pos[1]) == true
		&& (*this->_map)[-1 + pos[0]][1 + pos[1]] == 0
		&& (*this->_map)[-2 + pos[0]][2 + pos[1]] == player) { // e_DIAGUP
		result += 250000;
	}
	if (this->calc->good_val_full(2 + pos[0], -2 + pos[1]) == true
		&& (*this->_map)[1 + pos[0]][-1 + pos[1]] == 0
		&& (*this->_map)[2 + pos[0]][-2 + pos[1]] == player) { // e_DIAGUP
		result += 250000;
	}

	if (this->calc->good_val_full(-2 + pos[0], -2 + pos[1]) == true
		&& (*this->_map)[-1 + pos[0]][-1 + pos[1]] == 0
		&& (*this->_map)[-2 + pos[0]][-2 + pos[1]] == player) { // e_DIAGDOWN
		result += 250000;
	}
	if (this->calc->good_val_full(2 + pos[0], 2 + pos[1]) == true
		&& (*this->_map)[1 + pos[0]][1 + pos[1]] == 0
		&& (*this->_map)[2 + pos[0]][2 + pos[1]] == player) { // e_DIAGDOWN
		result += 250000;
	}


	if (this->calc->good_val_full(pos[0], -2 + pos[1]) == true
		&& (*this->_map)[pos[0]][-1 + pos[1]] == 0
		&& (*this->_map)[pos[0]][-2 + pos[1]] == adv) { // e_HORIZONTAL
		result += 200000;
	}
	if (this->calc->good_val_full(pos[0], 2 + pos[1]) == true
		&& (*this->_map)[pos[0]][1 + pos[1]] == 0
		&& (*this->_map)[pos[0]][2 + pos[1]] == adv) { // e_HORIZONTAL
		result += 200000;
	}

	if (this->calc->good_val_full(-2 + pos[0], pos[1]) == true
		&& (*this->_map)[-1 + pos[0]][pos[1]] == 0
		&& (*this->_map)[-2 + pos[0]][pos[1]] == adv) { // e_VERTICAL
		result += 200000;
	}
	if (this->calc->good_val_full(2 + pos[0], pos[1]) == true
		&& (*this->_map)[1 + pos[0]][pos[1]] == 0
		&& (*this->_map)[2 + pos[0]][pos[1]] == adv) { // e_VERTICAL
		result += 200000;
	}

	if (this->calc->good_val_full(-2 + pos[0], 2 + pos[1]) == true
		&& (*this->_map)[-1 + pos[0]][1 + pos[1]] == 0
		&& (*this->_map)[-2 + pos[0]][2 + pos[1]] == adv) { // e_DIAGUP
		result += 200000;
	}
	if (this->calc->good_val_full(2 + pos[0], -2 + pos[1]) == true
		&& (*this->_map)[1 + pos[0]][-1 + pos[1]] == 0
		&& (*this->_map)[2 + pos[0]][-2 + pos[1]] == adv) { // e_DIAGUP
		result += 200000;
	}

	if (this->calc->good_val_full(-2 + pos[0], -2 + pos[1]) == true
		&& (*this->_map)[-1 + pos[0]][-1 + pos[1]] == 0
		&& (*this->_map)[-2 + pos[0]][-2 + pos[1]] == adv) { // e_DIAGDOWN
		result += 200000;
	}
	if (this->calc->good_val_full(2 + pos[0], 2 + pos[1]) == true
		&& (*this->_map)[1 + pos[0]][1 + pos[1]] == 0
		&& (*this->_map)[2 + pos[0]][2 + pos[1]] == adv) { // e_DIAGDOWN
		result += 200000;
	}
	return (result);
}

// pos[0] = y; pos[1] = x 
int     Ia::score_tab( t_node *node ) {
	int                 result = 0,
						y = 0,
						x = 0,
						tmp = 1,
						player = 0;
	Map *               map = node->map;
	std::vector<int> *  center_map = node->pos;

	player = node->player;
	y = 1;
	while (this->calc->good_val((*center_map)[0] - y)
		&& (*map)[(*center_map)[0] - y][(*center_map)[1]] == player) {
		y++;
		tmp++;
	}
	y = 1;
	while (this->calc->good_val((*center_map)[0] + y)
		&& (*map)[(*center_map)[0] + y][(*center_map)[1]] == player) {
		y++;
		tmp++;
	}
	if (node->type == 1)
		tmp--;
	if (tmp > 1) {
		result += get_score(tmp, ALIGN, player);
	}

	x = 1;
	tmp = 1;
	while (this->calc->good_val((*center_map)[1] - x)
		&& (*map)[(*center_map)[0]][(*center_map)[1] - x] == player) {
		x++;
		tmp++;
	}
	x = 1;
	while (this->calc->good_val((*center_map)[1] + x)
		&& (*map)[(*center_map)[0]][(*center_map)[1] + x] == player) {
		x++;
		tmp++;
	}
	if (node->type == 1)
		tmp--;
	if (tmp > 1) {
		result += get_score(tmp, ALIGN, player);
	}

	y = 1;
	x = 1;
	tmp = 1;
	while (this->calc->good_val((*center_map)[0] - y) && this->calc->good_val((*center_map)[1] - x)
		&& (*map)[(*center_map)[0] - y][(*center_map)[1] - x] == player) {
		y++;
		x++;
		tmp++;
	}
	y = 1;
	x = 1;
	while (this->calc->good_val((*center_map)[0] + y) && this->calc->good_val((*center_map)[1] + x)
		&& (*map)[(*center_map)[0] + y][(*center_map)[1] + x] == player) {
		y++;
		x++;
		tmp++;
	}
	if (node->type == 1)
		tmp--;
	if (tmp > 1) {
		result += get_score(tmp, ALIGN, player);
	}
	
	y = 1;
	x = 1;
	tmp = 1;
	while (this->calc->good_val((*center_map)[0] - y) && this->calc->good_val((*center_map)[1] + x)
		&& (*map)[(*center_map)[0] - y][(*center_map)[1] + x] == player) {
		y++;
		x++;
		tmp++;
	}
	y = 1;
	x = 1;
	while (this->calc->good_val((*center_map)[0] + y) && this->calc->good_val((*center_map)[1] - x)
		&& (*map)[(*center_map)[0] + y][(*center_map)[1] - x] == player) {
		y++;
		x++;
		tmp++;
	}
	if (node->type == 1)
		tmp--;
	if (tmp > 1) {
		result += get_score(tmp, ALIGN, player);
	}
	result += eval_capture(*node->pos, (*node->pos)[0], (*node->pos)[1] + 3, (player % 2) + 1); // e_HORIZONTAL
	result += eval_capture(*node->pos, (*node->pos)[0], (*node->pos)[1] - 3, (player % 2) + 1); // e_HORIZONTAL
	
	result += eval_capture(*node->pos, (*node->pos)[0] + 3, (*node->pos)[1], (player % 2) + 1); // e_VERTICAL
	result += eval_capture(*node->pos, (*node->pos)[0] - 3, (*node->pos)[1], (player % 2) + 1); // e_VERTICAL

	result += eval_capture(*node->pos, (*node->pos)[0] - 3, (*node->pos)[1] + 3, (player % 2) + 1); // e_DIAGUP
	result += eval_capture(*node->pos, (*node->pos)[0] + 3, (*node->pos)[1] - 3, (player % 2) + 1); // e_DIAGUP

	result += eval_capture(*node->pos, (*node->pos)[0] + 3, (*node->pos)[1] + 3, (player % 2) + 1); // e_DIAGDOWN
	result += eval_capture(*node->pos, (*node->pos)[0] - 3, (*node->pos)[1] - 3, (player % 2) + 1); // e_DIAGDOWN

	result += eval_spaced_align(*node->pos);

	return (result);
}

// evaluate fct
int     Ia::calc_val(t_node *node) {
		int score;

		score = this->score_tab(node);
		return (score);
}

void    Ia::next_node( t_node * node ) {
	if (false == node->status)
		return;

	node->actuel++;
	if (node->actuel == node->child->end())
		node->status = false;
}

t_node *    Ia::get_child_node( t_node * node ) { return *node->actuel; }

void    Ia::get_rand_dir(std::vector<int> *pos, std::vector<int> *move, Map *map) {
	int x = 0;

	while (abs(x) < 15) {
		if (this->calc->good_val((*move)[0]) == true && this->calc->good_val((*move)[1] + x) == true
			&& (*map)[(*move)[0]][(*move)[1] + x] == 0) {
			(*pos)[0] = (*move)[0];
			(*pos)[1] = (*move)[1] + x;
			return ;
		}
		else if (this->calc->good_val((*move)[0]) == true && this->calc->good_val((*move)[1] - x) == true
			&& (*map)[(*move)[0]][(*move)[1] - x] == 0) {
			(*pos)[0] = (*move)[0];
			(*pos)[1] = (*move)[1] - x;
			return ;
		}
		else if (this->calc->good_val((*move)[0] + x) == true && this->calc->good_val((*move)[1]) == true
			&& (*map)[(*move)[0] + x][(*move)[1]] == 0) {
			(*pos)[0] = (*move)[0] + x;
			(*pos)[1] = (*move)[1];
			return ;
		}
		else if (this->calc->good_val((*move)[0] - x) == true && this->calc->good_val((*move)[1]) == true
			&& (*map)[(*move)[0] - x][(*move)[1]] == 0) {
			(*pos)[0] = (*move)[0] - x;
			(*pos)[1] = (*move)[1];
			return ;
		}
		else if (this->calc->good_val((*move)[0] + x) == true && this->calc->good_val((*move)[1] + x) == true
			&& (*map)[(*move)[0] + x][(*move)[1] + x] == 0) {
			(*pos)[0] = (*move)[0] + x;
			(*pos)[1] = (*move)[1] + x;
			return ;
		}
		else if (this->calc->good_val((*move)[0] + x) == true && this->calc->good_val((*move)[1] - x) == true
			&& (*map)[(*move)[0] + x][(*move)[1] - x] == 0) {
			(*pos)[0] = (*move)[0] + x;
			(*pos)[1] = (*move)[1] - x;
			return ;
		}
		else if (this->calc->good_val((*move)[0] - x) == true && this->calc->good_val((*move)[1] + x) == true
			&& (*map)[(*move)[0] - x][(*move)[1] + x] == 0) {
			(*pos)[0] = (*move)[0] - x;
			(*pos)[1] = (*move)[1] + x;
			return ;
		}
		else if (this->calc->good_val((*move)[0] - x) == true && this->calc->good_val((*move)[1] - x) == true
			&& (*map)[(*move)[0] - x][(*move)[1] - x] == 0) {
			(*pos)[0] = (*move)[0] - x;
			(*pos)[1] = (*move)[1] - x;
			return ;
		}
		x++;
	}
}

int     Ia::get_line_size(std::vector<int> pos, int y, int x, int player, Map *map) {
	int i = 0, j = 0, tmp = 0;
	bool state = true; // true if capture

	if (this->calc->good_val(pos[0]) != true || this->calc->good_val(x) != true
		|| this->calc->good_val(pos[1]) != true || this->calc->good_val(y) != true )
		return (0);

	if (pos[0] < y)
		j++;
	else if (pos[0] > y)
		j--;
	if (pos[1] < x)
		i++;
	else if (pos[1] > x)
		i--;

	while (state == true && (pos[0] + j != y || pos[1] + i != x)) {
		if ((pos[0] + j != y || pos[1] + i != x)
			&& (*map)[pos[0] + j][pos[1] + i] != player)
			state = false;
		else {
			tmp++;
			if (pos[0] < y)
				j++;
			else if (pos[0] > y)
				j--;
			if (pos[1] < x)
				i++;
			else if (pos[1] > x)
				i--;
		}
	}
	return (tmp);
}

void    Ia::get_dir(std::vector<int> *pos, std::vector<int> *move, Map *map, int player) {
	int     dir = 0, tmp = 0, good = 0;
	int     i = 0, j = 0;

	if (true == this->calc->good_val((*move)[0] - 1 ) && true == this->calc->good_val((*move)[1] + 1)
		&& player == (*this->_map)[(*move)[0] - 1 ][(*move)[1] + 1]) {
		tmp = get_line_size(*move, (*move)[0] - 4, (*move)[1] + 4, player, map);
		if (tmp > good) {
			good = tmp;
			dir = e_RIGHT_TOP;
		}
	}
	if (true == this->calc->good_val((*move)[1] + 1) && true == this->calc->good_val((*move)[0])
		&& player == (*this->_map)[(*move)[0]][(*move)[1] + 1]) {
		tmp = get_line_size(*move, (*move)[0], (*move)[1] + 4, player, map);
		if (tmp > good) {
			good = tmp;
			dir = e_RIGHT;
		}
	}
	if (true == this->calc->good_val((*move)[0] + 1) && true == this->calc->good_val((*move)[1] + 1)
		&& player == (*this->_map)[(*move)[0] + 1][(*move)[1] + 1]) {
		tmp = get_line_size(*move, (*move)[0] + 4, (*move)[1] + 4, player, map);
		if (tmp > good) {
			good = tmp;
			dir = e_RIGHT_BOTTOM;
		}
	}
	if (true == this->calc->good_val((*move)[0] + 1) && true == this->calc->good_val((*move)[1])
		&& player == (*this->_map)[(*move)[0] + 1][(*move)[1]]) {
		tmp = get_line_size(*move, (*move)[0] + 4, (*move)[1], player, map);
		if (tmp > good) {
			good = tmp;
			dir = e_BOTTOM;
		}
	}
	if (true == this->calc->good_val((*move)[0] + 1) && true == this->calc->good_val((*move)[1] - 1)
		&& player == (*this->_map)[(*move)[0] + 1][(*move)[1] - 1]) {
		tmp = get_line_size(*move, (*move)[0] + 4, (*move)[1] - 4, player, map);
		if (tmp > good) {
			good = tmp;
			dir = e_LEFT_BOTTOM;
		}
	}
	if (true == this->calc->good_val((*move)[1] - 1) && true == this->calc->good_val((*move)[0])
		&& player == (*this->_map)[(*move)[0]][(*move)[1] - 1]) {
		tmp = get_line_size(*move, (*move)[0], (*move)[1] - 4, player, map);
		if (tmp > good) {
			good = tmp;
			dir = e_LEFT;
		}
	}
	if (true == this->calc->good_val((*move)[0] - 1) && true == this->calc->good_val((*move)[1] - 1)
		&& player == (*this->_map)[(*move)[0] - 1][(*move)[1] - 1]) {
		tmp = get_line_size(*move, (*move)[0] - 4, (*move)[1] - 4, player, map);
		if (tmp > good) {
			good = tmp;
			dir = e_LEFT_TOP;
		}
	}
	if (true == this->calc->good_val((*move)[0] - 1) && true == this->calc->good_val((*move)[1])
		&& player == (*this->_map)[(*move)[0] - 1][(*move)[1]]) {
		tmp = get_line_size(*move, (*move)[0] - 4, (*move)[1], player, map);
		if (tmp > good) {
			good = tmp;
			dir = e_TOP;
		}
	}
	while (abs(i) < 6 && abs(j) < 6) {
		if (this->calc->good_val((*move)[0] + j) == true && this->calc->good_val((*move)[1] + i) == true
			&& (*map)[(*move)[0] + j][(*move)[1] + i] == 0) {
			(*pos)[0] = (*move)[0] + j;
			(*pos)[1] = (*move)[1] + i;
			return ;
		}
		if (this->calc->good_val((*move)[0] - j) == true && this->calc->good_val((*move)[1] - i) == true
			&& (*map)[(*move)[0] - j][(*move)[1] - i] == 0) {
			(*pos)[0] = (*move)[0] - j;
			(*pos)[1] = (*move)[1] - i;
			return ;
		}
		if (dir == e_RIGHT_TOP) {
			j--;
			i++;
		}
		else if (dir == e_RIGHT) {
			i++;
		}
		else if (dir == e_RIGHT_BOTTOM) {
			i++;
			j++;
		}
		else if (dir == e_BOTTOM) {
			j++;
		}
		else if (dir == e_LEFT_BOTTOM) {
			j++;
			i--;
		}
		else if (dir == e_LEFT) {
			i++;
		}
		else if (dir == e_LEFT_TOP) {
			i--;
			j--;
		}
		else {
			j++;
		}
	}
}

void    Ia::get_around(t_node *node, std::vector<int> *pos, int player, int type) {
	t_node 				* new_node;
	std::vector<int> 	* tmp = new std::vector<int>();
	if (node == NULL) {
		this->w_full("Malloc Error Ia::get_around: tmp");
		throw std::exception();
	}
	tmp->push_back(-1);
	tmp->push_back(-1);
	if (type == 1)
		get_dir(tmp, pos, node->map, (player % 2) + 1);
	else 
		get_dir(tmp, pos, node->map, player);

	if ((*tmp)[0] < 0 || (*tmp)[1] < 0)
		get_rand_dir(tmp, pos, node->map);

	new_node = get_new_node(node->pos_off, node->pos_def, node->pos_adv);
	new_node->player = player;
	copy_vect(new_node->pos, *tmp);
	new_node->map = copy_map(node->map);
	(*new_node->map)[(*tmp)[0]][(*tmp)[1]] = player;
	new_node->type = type;
	(*new_node->pos)[0] = (*tmp)[0];
	(*new_node->pos)[1] = (*tmp)[1];

	node->child->push_back(new_node);
	node->actuel = node->child->begin();

	tmp->clear();
}

void	Ia::addSingleNode(std::vector<int> pos, t_node *node, int player) {
	t_node * new_node;
	new_node = get_new_node(node->pos_off, node->pos_def, node->pos_adv);

	new_node->player = player;
	copy_vect(new_node->pos, pos);
	new_node->map = copy_map(node->map);
	(*new_node->map)[pos[0]][pos[1]] = player;
	if (player == HUMAN)
		new_node->type = 1;
	else
		new_node->type = 0;
	node->child->push_back(new_node);
	node->actuel = node->child->begin();
}

// build tree && evaluate
void    Ia::build_tree( int player, t_node *nodes, int direction, int depth ) {
	std::list<struct s_node *>::iterator    it;
	std::list<struct s_node *>::iterator    end;

	std::list<t_line *>::iterator    s_line;
	std::list<t_line *>::iterator    e_line;
	std::vector<int> tmp;
	(void)player;
	(void)nodes;
	(void)direction;

	if (depth >= ft42::cmd_depth)
		return ;
	if (depth == 0) {
		s_line = this->line_list.begin();
		e_line = this->line_list.end();

		while (s_line != e_line) {
			if ((*s_line)->type == 0) {
				tmp.push_back((*s_line)->pStart[0]);
				tmp.push_back((*s_line)->pStart[1]);
				if ((*s_line)->pStart_empty == true
					&& this->calc->good_val_full((*s_line)->pStart[0], (*s_line)->pStart[1]) == true
					&& (*this->_map)[(*s_line)->pStart[0]][(*s_line)->pStart[1]] == 0) {
					addSingleNode(tmp, nodes, (*s_line)->player);
				}
					
				tmp.clear();
				tmp.push_back((*s_line)->pEnd[0]);
				tmp.push_back((*s_line)->pEnd[1]);
				if ((*s_line)->pEnd_empty == true
					&& this->calc->good_val_full((*s_line)->pEnd[0], (*s_line)->pEnd[1]) == true
					&& (*this->_map)[(*s_line)->pEnd[0]][(*s_line)->pEnd[1]] == 0) {
					addSingleNode(tmp, nodes, (*s_line)->player);
				}
				tmp.clear();
			}
			else if ((*s_line)->type == 1
				&& (*this->_map)[(*s_line)->middle_pos[0]][(*s_line)->middle_pos[1]] == 0) {
				tmp.push_back((*s_line)->middle_pos[0]);
				tmp.push_back((*s_line)->middle_pos[1]);
				addSingleNode(tmp, nodes, (*s_line)->player);
				tmp.clear();
			}
			s_line++;
		}
	}
	else
		get_around(nodes, nodes->pos, player, 0);

	it = nodes->child->begin();
	end = nodes->child->end();

	while (it != end) {
		(*it)->node_type = ((depth % 2) == 0) ? 2 : 1;
		build_tree((player % 2) + 1, *it, check_around(*(*it)->pos), depth + 1);
		it++;
	}
}

void		Ia::free_nodes(t_node *toDelete) {
	if (toDelete != NULL) {
		toDelete->actuel = toDelete->child->begin();
		if (toDelete->child->size() > 0)
			toDelete->status = true;
		while (toDelete->status != false) {
			free_nodes(get_child_node(toDelete));
			next_node(toDelete);
		}
		if (toDelete->map != NULL) {
			std::vector< std::vector<int> >::iterator	it = toDelete->map->begin();
			std::vector< std::vector<int> >::iterator	end = toDelete->map->end();
			while (it != end) {
				(*it).clear();
				it++;
			}
			toDelete->map->clear();
			delete toDelete->map;
		}
		if (toDelete->pos != NULL) {
			toDelete->pos->clear();
			delete toDelete->pos;
		}
		if (toDelete->pos_off != NULL) {
			toDelete->pos_off->clear();
			delete toDelete->pos_off;
		}
		if (toDelete->pos_def != NULL) {
			toDelete->pos_def->clear();
			delete toDelete->pos_def;
		}
		if (toDelete->pos_adv != NULL) {
			toDelete->pos_adv->clear();
			delete toDelete->pos_adv;
		}
		if (toDelete->child != NULL) {
			toDelete->child->clear();
			delete toDelete->child;
		}
		std::free(toDelete);
		this->w_log("FREE Ia::free_nodes");
	}
}

t_node *    Ia::get_new_node( std::vector<int> *off, std::vector<int> *def, std::vector<int> *adv ) {
	t_node * node = static_cast<t_node *>(malloc(sizeof(t_node)));
	if (node == NULL) {
		this->w_full("Malloc Error Ia::get_new_nodeon:");
		throw std::exception();
	}
	node->map = NULL;
	node->pos = new std::vector<int>();
	node->pos_off = new std::vector<int>();
	node->pos_def = new std::vector<int>();
	node->pos_adv = new std::vector<int>();
	node->child = new std::list<t_node *>();
	if (node->pos == NULL || node->pos_off == NULL || node->pos_def == NULL || node->pos_adv == NULL || node->child == NULL) {
		this->w_full("Malloc Error Ia::get_new_nodeon: node->pos, node->pos_off, node->pos_def, node->pos_adv or node->child ");
		throw std::exception();
	}
	node->pos->push_back(-1);
	node->pos->push_back(-1);
	node->pos_off->push_back(-1);
	node->pos_off->push_back(-1);
	node->pos_def->push_back(-1);
	node->pos_def->push_back(-1);
	node->pos_adv->push_back(-1);
	node->pos_adv->push_back(-1);
	node->status = true;
	node->node_type = 1;
	node->cost = 0;
	if (off != NULL)
		copy_vect(node->pos_off, *off);
	if (def != NULL)
		copy_vect(node->pos_def, *def);
	if (adv != NULL)
		copy_vect(node->pos_adv, *adv);
	return (node);
}

void	Ia::set_node_status( t_node * node ) {
	if (node->child->size() > 0) {
		node->status = true;
		node->actuel = node->child->begin();
		if (node->actuel == node->child->end())
			node->status = false;
		return ;
	}
	node->status = false;

}

void    Ia::copy_vect(std::vector<int> *dest, std::vector<int> src) {
	(*dest)[0] = src[0];
	(*dest)[1] = src[1];
}

bool	is_remove(const t_line * line) {
	if (line->pStart_empty == false && line->pEnd_empty == false)
		return true;
	return false;
}

void    Ia::do_calc_node( t_node * node ) {
	t_node * 	tmp;
	t_node * 	good;
	int 		i = 0, val = -INF;  // debug
	bool		play = false;

	set_node_status(node);
	while (node->status != false) {
		tmp = get_child_node(node);
		tmp->cost = alphabeta(tmp, -INF, INF);
		if (tmp->cost > val
			&& this->calc->good_val_full((*tmp->pos)[0], (*tmp->pos)[1])
			&& (*this->_map)[(*tmp->pos)[0]][(*tmp->pos)[1]] == 0 ) {
			val = tmp->cost;
			good = tmp;
			play = true;
		}
		if (ft42::iathink == true
			&& this->calc->good_val_full((*tmp->pos)[0], (*tmp->pos)[1]) == true
			&& (*this->_map)[(*tmp->pos)[0]][(*tmp->pos)[1]] == 0) {
			this->serialize((*tmp->pos)[1], (*tmp->pos)[0], tmp->cost);
		}
		next_node(node);
		i++;
	}
	if (play == false) { // may add this pos the list !!
		play_rand();
		return ;
	}
	if (ft42::two_players == false) {
		this->main_event->put_stone(*good->pos);
		this->main_event->render->old_ia_pos[0] = (*good->pos)[0];
		this->main_event->render->old_ia_pos[1] = (*good->pos)[1];
		if (this->calc->good_val((*good->pos)[0]) == true && this->calc->good_val((*good->pos)[1]) == true)
			this->checkAlign(good->pos);
		std::list<t_line *>::iterator   it = this->line_list.begin();
		std::list<t_line *>::iterator   end = this->line_list.end();
		while (it != end) {
			if (pos_compare(good->pos, (*it)->pStart) == 1) {
				(*it)->pStart_empty = false;
			}
			else if ( pos_compare(good->pos, (*it)->pEnd) == 1) {
				(*it)->pEnd_empty = false;
			}
			it++;
		}
		if (good->type == 0) // off
			copy_vect(this->pos_off, *good->pos);
		else // def
			copy_vect(this->pos_def, *good->pos);
		free_nodes(node);
	}
	else {
		this->suggestion[0] = (*good->pos)[0];
		this->suggestion[1] = (*good->pos)[1];
	}
}

void 	Ia::play_rand( void ) {
	if (this->calc->good_val_full((*this->pos_adv)[0], (*this->pos_adv)[1]) == true) {
		if (this->calc->good_val_full(1 + (*this->pos_adv)[1], -1 + (*this->pos_adv)[0]) == true
		&& (*this->_map)[ -1 + (*this->pos_adv)[0] ][ 1 + (*this->pos_adv)[1] ] == 0) {
		this->pos[0] = -1 + (*this->pos_adv)[0];
		this->pos[1] = 1 + (*this->pos_adv)[1];
		}
		else if (this->calc->good_val_full(1 + (*this->pos_adv)[1], (*this->pos_adv)[0]) == true
			&& (*this->_map)[ (*this->pos_adv)[0] ][ 1 + (*this->pos_adv)[1] ] == 0) {
			this->pos[0] = (*this->pos_adv)[0];
			this->pos[1] = 1 + (*this->pos_adv)[1];
		}
		else if (this->calc->good_val_full(1 + (*this->pos_adv)[1], 1 + (*this->pos_adv)[0]) == true
			&& (*this->_map)[ 1 + (*this->pos_adv)[0] ][ 1 + (*this->pos_adv)[1] ] == 0) {
			this->pos[0] = 1 + (*this->pos_adv)[0];
			this->pos[1] = 1 + (*this->pos_adv)[1];
		}
		else if (this->calc->good_val_full((*this->pos_adv)[1], 1 + (*this->pos_adv)[0]) == true
			&& (*this->_map)[ 1 + (*this->pos_adv)[0] ][ (*this->pos_adv)[1] ] == 0) {
			this->pos[0] = 1 + (*this->pos_adv)[0];
			this->pos[1] = (*this->pos_adv)[1];
		}
		else if (this->calc->good_val_full(-1 + (*this->pos_adv)[1], 1 + (*this->pos_adv)[0]) == true
			&& (*this->_map)[ 1 + (*this->pos_adv)[0] ][ -1 + (*this->pos_adv)[1] ] == 0) {
			this->pos[0] = 1 + (*this->pos_adv)[0];
			this->pos[1] = -1 + (*this->pos_adv)[1];
		}
		else if (this->calc->good_val_full(-1 + (*this->pos_adv)[1], (*this->pos_adv)[0]) == true
			&& (*this->_map)[ (*this->pos_adv)[0] ][ -1 + (*this->pos_adv)[1] ] == 0) {
			this->pos[0] = (*this->pos_adv)[0];
			this->pos[1] = -1 + (*this->pos_adv)[1];
		}
		else if (this->calc->good_val_full(-1 + (*this->pos_adv)[1], -1 + (*this->pos_adv)[0]) == true
			&& (*this->_map)[ -1 + (*this->pos_adv)[0] ][ -1 + (*this->pos_adv)[1] ] == 0) {
			this->pos[0] = -1 + (*this->pos_adv)[0];
			this->pos[1] = -1 + (*this->pos_adv)[1];
		}
		else if (this->calc->good_val_full((*this->pos_adv)[1], -1 + (*this->pos_adv)[0]) == true
			&& (*this->_map)[(*this->pos_adv)[0] ][ 1 + (*this->pos_adv)[1] ] == 0) {
			this->pos[0] = -1 + (*this->pos_adv)[0];
			this->pos[1] = (*this->pos_adv)[1];
		}
	}
	else {
		this->pos[0] = rand() % 19;
		this->pos[1] = rand() % 19;
		while (calc->check_pos(this->pos, 2) != true) {
			this->pos[0] = rand() % 19;
			this->pos[1] = rand() % 19;
		}
	}
	if (ft42::two_players == false) {
		copy_vect(this->pos_off, this->pos);
		this->main_event->put_stone(this->pos);
		this->checkAlign(&this->pos);
		this->main_event->render->old_ia_pos[0] = this->pos[0];
		this->main_event->render->old_ia_pos[1] = this->pos[1];
	}
	else {
		this->suggestion[0] = this->pos[0];
		this->suggestion[1] = this->pos[1];
	}
}

void	Ia::play_where_capture( void ) {
	std::vector<int> pos;

	if ((*this->_map)[this->main_event->line_winner.p1[0]][this->main_event->line_winner.p1[1]] == 0) {
		if (two_players == false) {
			pos.push_back(this->main_event->line_winner.p1[0]);
			pos.push_back(this->main_event->line_winner.p1[1]);
			this->main_event->put_stone(pos);
			checkAlign(&pos);
			pos.clear();
		}
		else {
			this->suggestion[0] = this->main_event->line_winner.p1[0];
			this->suggestion[1] = this->main_event->line_winner.p1[1];
		}
	}
	else if ((*this->_map)[this->main_event->line_winner.p2[0]][this->main_event->line_winner.p2[1]] == 0) {
		if (two_players == false) {
			pos.push_back(this->main_event->line_winner.p2[0]);
			pos.push_back(this->main_event->line_winner.p2[1]);
			this->main_event->put_stone(pos);
			checkAlign(&pos);
			pos.clear();
		}
		else {
			this->suggestion[0] = this->main_event->line_winner.p2[0];
			this->suggestion[1] = this->main_event->line_winner.p2[1];
		}
	}
	else if ((*this->_map)[this->main_event->line_winner.p3[0]][this->main_event->line_winner.p3[1]] == 0) {
		if (two_players == false) {
			pos.push_back(this->main_event->line_winner.p3[0]);
			pos.push_back(this->main_event->line_winner.p3[1]);
			this->main_event->put_stone(pos);
			checkAlign(&pos);
			pos.clear();
		}
		else {
			this->suggestion[0] = this->main_event->line_winner.p3[0];
			this->suggestion[1] = this->main_event->line_winner.p3[1];
		}
	}
	else if ((*this->_map)[this->main_event->line_winner.p4[0]][this->main_event->line_winner.p4[1]] == 0) {
		if (two_players == false) {
			pos.push_back(this->main_event->line_winner.p4[0]);
			pos.push_back(this->main_event->line_winner.p4[1]);
			this->main_event->put_stone(pos);
			checkAlign(&pos);
			pos.clear();
		}
		else {
			this->suggestion[0] = this->main_event->line_winner.p4[0];
			this->suggestion[1] = this->main_event->line_winner.p4[1];
		}
	}
	else {
		if (two_players == false) {
			pos.push_back(this->main_event->line_winner.p5[0]);
			pos.push_back(this->main_event->line_winner.p5[1]);
			this->main_event->put_stone(pos);
			checkAlign(&pos);
			pos.clear();
		}
		else {
			this->suggestion[0] = this->main_event->line_winner.p5[0];
			this->suggestion[1] = this->main_event->line_winner.p5[1];
		}
	}
	ft42::win_i = 0;
}

void    Ia::play( int player, std::vector<int> pos) {
	ft42::node_score.clear();
	copy_vect(this->pos_adv, pos);
	t_node * node = NULL;
	int     direction = this->check_around(pos);

	if (this->calc->good_val(pos[0]) == true && this->calc->good_val(pos[1]) == true)
		this->checkAlign(&pos);
	if (ft42::win_i == 1) {
		play_where_capture();
	}
	else if (this->line_list.size() > 0) {
		node = get_new_node(this->pos_off, this->pos_def, this->pos_adv);
		node->map = copy_map(this->_map);
		build_tree(player, node, direction, 0);
		do_calc_node(node);
	}
	else {
		play_rand();
	}
}

int     Ia::check_around(std::vector<int> pos) {
	if (this->calc->good_val(pos[0]) == false || this->calc->good_val(pos[1]) == false)
		return 0;
	int player = (*this->_map)[pos[0]][pos[1]];
	if (player == 0)
		player = 1;
	if (true == this->calc->good_val(pos[0] - 1 ) && true == this->calc->good_val(pos[1] + 1)
		&& player == (*this->_map)[pos[0] - 1 ][pos[1] + 1])
		return e_RIGHT_TOP;
	if (true == this->calc->good_val(pos[1] + 1) && true == this->calc->good_val(pos[0])
		&& player == (*this->_map)[pos[0]][pos[1] + 1])
		return e_RIGHT;
	if (true == this->calc->good_val(pos[0] + 1) && true == this->calc->good_val(pos[1] + 1)
		&& player == (*this->_map)[pos[0] + 1][pos[1] + 1])
		return e_RIGHT_BOTTOM;
	if (true == this->calc->good_val(pos[0] + 1) && true == this->calc->good_val(pos[1])
		&& player == (*this->_map)[pos[0] + 1][pos[1]])
		return e_BOTTOM;
	if (true == this->calc->good_val(pos[0] + 1) && true == this->calc->good_val(pos[1] - 1)
		&& player == (*this->_map)[pos[0] + 1][pos[1] - 1])
		return e_LEFT_BOTTOM;
	if (true == this->calc->good_val(pos[1] - 1) && true == this->calc->good_val(pos[0])
		&& player == (*this->_map)[pos[0]][pos[1] - 1])
		return e_LEFT;
	if (true == this->calc->good_val(pos[0] - 1) && true == this->calc->good_val(pos[1] - 1)
		&& player == (*this->_map)[pos[0] - 1][pos[1] - 1])
		return e_LEFT_TOP;
	if (true == this->calc->good_val(pos[0] - 1) && true == this->calc->good_val(pos[1])
		&& player == (*this->_map)[pos[0] - 1][pos[1]])
		return e_TOP;
	return 0;
}

int     Ia::alphabeta(t_node *p, int alpha, int beta) {
	int best = 0, val = 0;

	if (p->child->size() <= 0)
		return (calc_val(p));
	else {
		best = -INF;
		set_node_status(p);
		while (p->status != false) {
			val = -alphabeta(get_child_node(p), -beta, -alpha);
			if (val > best) {
				best = val;
				if (best >= alpha) {
					alpha = best;
					if (alpha >= beta)
						return (best);
				}
			}
			next_node(p);
		}
		return (best);
	}
}
