#include <Render.class.hpp>

void	Render::draw_iathink() {
	if (true == ft42::iathink) {
		std::list<char *>::iterator it = ft42::node_score.begin();
		std::list<char *>::iterator end = ft42::node_score.end();
		int 						i = 1,
									x = 70,
									y = 750;

		while ( it != end && y <= this->height) {
			this->printString(x, y, *it);
			x += 140;
			if (i % 9 == 0) {
				x = 70;
				y += 50;
			}
			it++;
			i++;
		}
	}
}

void	Render::drawLegend( void ) {
	int			legend_x = this->width + 30,
				value = this->width;
	std::string	tmp;

	draw_circle(legend_x + 10, SCORE_P1_Y - 10, this->ring_size, BLUE_P1); // Human Circle
	draw_circle(legend_x + 10, SCORE_P2_Y - 10, this->ring_size, RED_P2); // IA circle
	draw_circle(legend_x + 10, TO_PLAY_Y - 10, (this->ring_size * 2), (this->main_event->get_player())); // Player turn circle
	tmp = std::to_string(this->main_event->gameState.tokenHumanCaptured);
	this->printString(value, SCORE_P1_Y, (char *)tmp.c_str()); // Score Human Player
	this->printString(legend_x + 40, SCORE_P1_Y, (char *)SCORE_P1); // Human Points (IA tokens Captured)
	tmp = std::to_string(this->main_event->gameState.tokenIaCaptured);
	this->printString(value, SCORE_P2_Y, (char *)tmp.c_str()); // VALUE IA Player
	if (true == ft42::two_players)
		this->printString(legend_x + 40, SCORE_P2_Y, (char *)SCORE_P2_PLAYER); // P2 Points (Human tokens Captured)
	else {
		this->printString(legend_x + 40, SCORE_P2_Y, (char *)SCORE_P2); // IA Points (Human tokens Captured)
		tmp = std::to_string((this->_time_end.tv_usec - this->_time_start.tv_usec));
		this->printString(legend_x + 80, IA_TIME_PLAY_Y, (char *)IA_TIME_PLAY); //Last IA's Duration to Play
		if (tmp[0] == '-')
			this->printString(value, IA_TIME_PLAY_Y, (char *)"1 sec"); // IA time
		else
			this->printString(value, IA_TIME_PLAY_Y, (char *)tmp.substr(0, 6).c_str()); // IA time
	}
	this->printString(legend_x + 40, TO_PLAY_Y, (char *)TO_PLAY); // 's turn'
	if (this->main_event->gameState.winner == 1) {
		this->printString(value, WINNER_Y, (char *)"Player 1"); //Winner
		this->printString(legend_x + 40, WINNER_Y, (char *)WINNER); //Winner
		this->drawWinnerLine();
	}
	else if (this->main_event->gameState.winner == 2) {
		if (true == ft42::two_players)
			this->printString(value, WINNER_Y, (char *)"Player 2"); //Winner
		else
			this->printString(value, WINNER_Y, (char *)"IA"); //Winner
		this->printString(legend_x + 40, WINNER_Y, (char *)WINNER); //Winner
		this->drawWinnerLine();
	}
	this->printString(30, 20, (char *)COLUMN_LINE); //Legend Column top
	this->leftLegendLine(10, 44); //legend line leftß
	this->draw_iathink();
}

void	Render::drawWinnerLine( void ) {
	draw_circle((1 + this->main_event->line_winner.p1[1]) * this->case_size,
		(1 + this->main_event->line_winner.p1[0]) * this->case_size, this->ring_size, WINNER_P);
	draw_circle((1 + this->main_event->line_winner.p2[1]) * this->case_size,
		(1 + this->main_event->line_winner.p2[0]) * this->case_size, this->ring_size, WINNER_P);
	draw_circle((1 + this->main_event->line_winner.p3[1]) * this->case_size,
		(1 + this->main_event->line_winner.p3[0]) * this->case_size, this->ring_size, WINNER_P);
	draw_circle((1 + this->main_event->line_winner.p4[1]) * this->case_size,
		(1 + this->main_event->line_winner.p4[0]) * this->case_size, this->ring_size, WINNER_P);
	draw_circle((1 + this->main_event->line_winner.p5[1]) * this->case_size,
		(1 + this->main_event->line_winner.p5[0]) * this->case_size, this->ring_size, WINNER_P);
}

void	Render::drawStatusMEssage( int legend_x ) {
	switch (this->main_event->gameState.statusMessage) {
		case e_GAME_INITIALIZED:	{ this->printString(legend_x, STA_LINE_1, (char *)STA_GAME_INIT); break; }
		case e_CAPTURE_IA:			{ this->printString(legend_x, STA_LINE_1, (char *)STA_IA_CAPTURE); break; }
		case e_CAPTURE_HUMAN:		{ this->printString(legend_x, STA_LINE_1, (char *)STA_PLAYER_CAPTURE); break; }
		case e_YOUR_TURN:			{ this->printString(legend_x, STA_LINE_1, (char *)STA_YOUR_TURN); break; }
		case e_CANT_PLACE:			{ this->printString(legend_x, STA_LINE_1, (char *)STA_CANT_PLACE); break; }
		case e_GAME_END_QUESTION:	{
										this->printString(legend_x, STA_LINE_1, (char *)STA_GAME_END_1);
										this->printString(legend_x, STA_LINE_1 + 50, (char *)STA_GAME_END_2);
										break;
									}
		default:					break;
	}
}

void	Render::leftLegendLine( int x, int y ) {
	int 		i = 0;
	std::string s ;

	while (i < 19) {
		s = std::to_string(i);
		if (i > 9)
			this->printString((x - 9), y + (i * 36), (char *)s.c_str());
		else
			this->printString(x, y + (i * 36), (char *)s.c_str());
		i++;
	}
}

void	Render::printString( int x, int y, char *str) {
	int	i = 0,
		len = strlen(str);

	glColor3f(0.0f, 0.0f, 0.0f);
	glRasterPos2i(x, y);

	glDisable(GL_TEXTURE_2D);
	while(i < len) {
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, str[i]);
		i++;
	}
	glEnable(GL_TEXTURE_2D);
}

void  	Render::keyboard( void ) {
	while (SDL_PollEvent(&this->event)) {
		if (this->event.type == SDL_KEYDOWN) {
			switch((this->event).key.keysym.sym) {
				case SDLK_ESCAPE:		{
					this->w_log("Key ECHAP");
					this->main_event->run = false;
				} break;
				default: break;
			}
		}
		else if (this->event.type == SDL_MOUSEBUTTONUP) {
			switch((this->event).button.button) {
				case SDL_BUTTON_LEFT:	{
					if (this->main_event->run == true)
						this->changeCaseEvent();
					break;
				}
				default: break;
			}
		}
	}
}

void	Render::changeCaseEvent(void) {
	this->case_event.x = -1;
	this->case_event.y = -1;
	if (this->event.button.x % this->case_size <= this->case_size / 4
		|| this->event.button.x % this->case_size >= (this->case_size / 4) * 3) {
		if (this->event.button.x % this->case_size >= this->case_size / 2)
			this->case_event.x = (this->event.button.x / this->case_size) + 1;
		else
			this->case_event.x = this->event.button.x / this->case_size;

		if (this->event.button.y % this->case_size >= this->case_size / 2)
			this->case_event.y = (this->event.button.y / this->case_size) + 1;
		else
			this->case_event.y = this->event.button.y / this->case_size;
	}
	if (this->case_event.x <= 0 || this->case_event.x > SIZE
		|| this->case_event.y <= 0 || this->case_event.y > SIZE) {
		this->case_event.x = -1;
		this->case_event.y = -1;
	}
	this->case_event.x -= 1;
	this->case_event.y -= 1;
}

void	Render::map_debugg( void ) {
	int					x,
						y = 0;
	std::vector<int>	line;

	this->debugg_map = new Map();
	while (y < SIZE) {
		x = 0;
		while (x < SIZE) {
			if (x % 2 == 0)
				line.push_back(1);
			else
				line.push_back(2);
			x++;
		}
		this->debugg_map->push_back(line);
		line.clear();
		y++;
	}
}


void	Render::draw_scene(Map * map) {
	int x = 0,
		y = 1;

	SDL_SetRenderDrawColor( this->renderer, 245, 245, 245, 0 );
	SDL_RenderClear( this->renderer );
	this->keyboard();
	SDL_Delay(50);
	while (y <= 19) {
		x = 1;
		while (x <= 19) {
			draw_circle(x * this->case_size, y * this->case_size, this->ring_size,
				(*map)[y - 1][x - 1]);
			x++;
		}
		y++;
	}
	if (RED_P2 == this->main_event->get_player()) {
		draw_circle((this->main_event->ia->suggestion[1] + 1) * this->case_size,
			(this->main_event->ia->suggestion[0] + 1) * this->case_size,
			this->ring_size, SUGGESTION);
	}
	if (true == ft42::draw_last_played && false == ft42::two_players
		&& this->main_event->calc->good_val_full( this->main_event->ia->pos[0],
		this->main_event->ia->pos[1])) {

		draw_circle((this->old_ia_pos[1] + 1) * this->case_size,
					(this->old_ia_pos[0] + 1) * this->case_size,
					this->ring_size,
					LASTPLAYED); // Player turn circle
	}
	draw_circle(1 * this->case_size, 1 * this->case_size, this->ring_size, (*map)[0][0]);
	this->drawLegend();
	SDL_RenderPresent( this->renderer );
}

void	Render::draw_circle( int posX, int posY, int radius, int player ) {
	int x = radius,
		y = 0,
		xChange = 1 - (radius << 1),
		yChange = 0,
		radiusError = 0;

	if (BLUE_P1 == player)
		glColor3f(0.0f, 0.0f, 1.0f);
	else if (RED_P2 == player)
		glColor3f(1.0f, 0.0f, 0.0f);
	else if (LASTPLAYED == player && true == ft42::draw_last_played && false == ft42::two_players) {
		glColor3f(1.0f, 1.0f, 0.0f);	}
	else if (WINNER_P == player)
		glColor3f(0.0f, 0.0f, 0.0f);
	else if (SUGGESTION == player && true == ft42::two_players)
		glColor3f(0.74f, 0.23f, 0.65f);
	else
		glColor3f(1.0f,1.0f, 1.0f);
	while (x >= y) {
		for (int i = posX - x; i <= posX + x; i++) {
			SDL_RenderDrawPoint(this->renderer, i, posY + y);
			SDL_RenderDrawPoint(this->renderer, i, posY - y);
		}
		for (int i = posX - y; i <= posX + y; i++) {
			SDL_RenderDrawPoint(this->renderer, i, posY + x);
			SDL_RenderDrawPoint(this->renderer, i, posY - x);
		}
		y++;
		radiusError += yChange;
		yChange += 2;
		if (((radiusError << 1) + xChange) > 0) {
			x--;
			radiusError += xChange;
			xChange += 2;
		}
	}
}

void	Render::init( Event * main_event, int ac, char ** av ) {
	if (true == ft42::iathink)
		this->height = 1340;
	else
		this->height = HEIGHT;
	glutInit(&ac, av);
	this->case_event.x = -1;
	this->case_event.y = -1;
	this->main_event = main_event;
	this->main_event->run = true;
	gettimeofday(&this->_time_start, NULL);
	gettimeofday(&this->_time_end, NULL);
	if (SDL_Init(SDL_INIT_VIDEO) < 0) { // Initialize SDL2
		this->w_full("Render::init SDL_Init error");
		throw std::exception();
	}
	this->window = SDL_CreateWindow(
		"Gomoku 42 jmoiroux & vjacquie",   // window title
		SDL_WINDOWPOS_UNDEFINED,           // initial x position
		0,           					   // initial y position
		(this->width + LEG_WIDTH),         			// width, in pixels
		this->height,                      // height, in pixels
		SDL_WINDOW_OPENGL                  // flags - see below
	);
	if (NULL == this->window) {
		this->w_full("Render::init Create windows error");
		throw std::exception();
	}
	this->renderer = SDL_CreateRenderer(this->window, -1, SDL_RENDERER_ACCELERATED);
	if ( NULL == this->renderer ) {
		this->w_full("Render::init Create renderer error");
		std::cout << "Failed to create renderer : " << SDL_GetError();
		throw std::exception();
	}
}

Render::Render() : window(NULL), renderer(NULL), mouse_position(NULL),
					width(WIDTH), height(HEIGHT), case_size(WIDTH / 20),
					ring_size(WIDTH / (3 * SIZE)), run(true) {}

Render::~Render() {
	SDL_DestroyRenderer(this->renderer);
	SDL_DestroyWindow(this->window);
	SDL_Quit();
}
