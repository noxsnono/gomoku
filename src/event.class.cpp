#include <event.class.hpp>
#include <main.hpp>

Event::Event( void ) : run(true) {
	srand(time(NULL));
	this->_player = (rand() % 2) + 1;
	this->_pos.push_back(-1);
	this->_pos.push_back(-1);
	this->_capture.push_back(0);
	this->_capture.push_back(0);
}

Event::Event( Event const & src ) { *this = src; }

Event & Event::operator=( Event const & rhs ) {
	if (this != &rhs) {}
	return (*this);
}

Event::~Event( void ) {}

int 	Event::get_player() { return this->_player; }

void	Event::parse_command(int ac, char **av) {
	int i = 0;

	ft42::cmd_depth = 1;
	ft42::two_players = false;
	ft42::draw_last_played = false;
	ft42::entry_d = -1;
	while ( i < ac ) {
		if ( 0 == strcmp(av[i], "-iathink") )
			ft42::iathink = true;
		else if ( 0 == strcmp(av[i], "-log") )
			ft42::logg = true;
		else if ( 0 == strcmp(av[i], "-2p") )
			ft42::two_players = true;
		else if ( 0 == strcmp(av[i], "-lastmove") )
			ft42::draw_last_played = true;
		else if ( 0 == strncmp(av[i], "-depth=", 7) ) {
			if ( strlen(av[i]) >= 7 && 1 == isdigit(av[i][7]) ) {
				ft42::cmd_depth = atoi(&av[i][7]);
				ft42::entry_d = ft42::cmd_depth;
				if (ft42::cmd_depth > 10)
					ft42::cmd_depth = 10;
				else if (ft42::cmd_depth <= 1)
					ft42::cmd_depth = 1;
				ft42::cmd_depth = 10 - ft42::cmd_depth + 1;
			}
		}
		i++;
	}
}

void	Event::init( int ac, char **av ) {
	this->parse_command(ac, av);

	this->init_map();
	this->calc = new Calc(this->map);
	if (this->calc == NULL) {
		this->w_full("Malloc Error Event::init: this->calc");
		throw std::exception();
	}
	this->calc->init(this, ac, av);
	this->render = new Render();
	if (this->render  == NULL) {
		this->w_full("Malloc Error Event::init: this->render");
		throw std::exception();
	}
	this->render->init(this, ac, av);
	this->ia = new Ia(this->map);
	if (this->ia == NULL) {
		this->w_full("Malloc Error Event::init: this->ia");
		throw std::exception();
	}
	this->ia->init(this, this->calc);
	this->gameState.winner = 0;
	this->gameState.statusMessage = e_GAME_INITIALIZED;
	this->gameState.predictive = 0;
	this->gameState.tokenIaCaptured = 0;
	this->gameState.tokenHumanCaptured = 0;
}

void	Event::init_map( void ) {
	int 				x, y = 0;
	std::vector<int>	line;

	this->map = new Map();
	if (this->map == NULL) {
		this->w_full("Malloc Error Event::init_map");
		throw std::exception();
	}

	while (y < SIZE) {
		x = 0;
		while (x < SIZE) {
			line.push_back(0);
			x++;
		}
		this->map->push_back(line);
		line.clear();
		y++;
	}
}

void	Event::exit_free( void ) {
	if (this->calc != NULL) {
		this->calc->main_event = NULL;
		this->calc->_map = NULL;
		delete this->calc;
		this->w_log("DELETE Calc");
	}
	if (this->render != NULL) {
		this->render->main_event = NULL;
		if (this->render->debugg_map != NULL ) {
			delete this->render->debugg_map;
			this->w_log("FREE this->render->debugg_map");
		}
		if (this->render != NULL) {
			delete this->render;
			this->w_log("DELETE Render");
		}
	}
	if (this->ia != NULL) {
		this->ia->main_event = NULL;
		if (this->ia->pos_off != NULL) {
			delete this->ia->pos_off;
			this->w_log("FREE this->ia->pos_off");
		}
		if (this->ia->pos_def != NULL) {
			delete this->ia->pos_def;
			this->w_log("FREE this->ia->pos_def");
		}
		if (this->ia->pos_adv != NULL) {
			delete this->ia->pos_adv;
			this->w_log("FREE this->ia->pos_adv");
		}
		if (this->ia->line_list.size() > 0) {
			t_line * 						tmp = NULL;
			std::list<t_line *>::iterator 	it = this->ia->line_list.begin();
			std::list<t_line *>::iterator 	end = this->ia->line_list.end();

			while (it != end) {
				tmp = *it;
				it++;
				if (tmp != NULL) {
					std::free(tmp);
				}
			}
			this->ia->line_list.clear();
			this->w_log("CLEAR this->ia->line_list");
		}
		delete this->ia;
	}
	if (this->map != NULL) {
		std::vector< std::vector<int> >::iterator	it = this->map->begin();
		std::vector< std::vector<int> >::iterator	end = this->map->end();

		while (it != end) {
			it->clear();
			it++;
		}
		this->map->clear();
		delete this->map;

	}
	this->w_log("Event::exit_free ==> End of free gomoku");
}

bool	Event::get_move( t_case * coord ) {
	if (false == this->calc->good_val_full(coord->y, coord->x))
		return (false);
	this->_pos[0] = coord->y;
	this->_pos[1] = coord->x;
	coord->y = -1;
	coord->x = -1;
	if (this->calc->check_pos(this->_pos, this->_player) == false)
		return (false);
	this->put_stone(this->_pos);
	return (true);
}

void	Event::put_stone( std::vector<int> pos ) {
	(*this->map)[pos[0]][pos[1]] = this->_player;
	this->_capture[this->_player - 1] += this->calc->check_capture(this->_player, pos);
	if (this->calc->check_win(this->_player, pos) == true) {
		this->run = false;
		this->gameState.winner = this->_player;
	}
	if (this->_capture[this->_player - 1] >= 10) {
		this->run = false;
		this->gameState.winner = this->_player;
	}
	this->gameState.tokenIaCaptured = this->_capture[1]; // IA
	this->gameState.tokenHumanCaptured = this->_capture[0]; // HUMAN
	this->_player = (this->_player == 1) ? 2 : 1; // change player's turn
}

bool	Event::check_if_5_present(void) {
	if (this->_player != this->line_winner.player) {
		return (false);
	}
	if ((*this->map)[this->line_winner.p1[0]][this->line_winner.p1[1]] == this->line_winner.player
		&& (*this->map)[this->line_winner.p2[0]][this->line_winner.p2[1]] == this->line_winner.player
		&& (*this->map)[this->line_winner.p3[0]][this->line_winner.p3[1]] == this->line_winner.player
		&& (*this->map)[this->line_winner.p4[0]][this->line_winner.p4[1]] == this->line_winner.player
		&& (*this->map)[this->line_winner.p5[0]][this->line_winner.p5[1]] == this->line_winner.player) {
		this->gameState.winner = this->line_winner.player;
		return (true);
	}
	else {
		ft42::win_align = false;
		ft42::win_i = 1;
	}
	return (false);
}

void	Event::lauchGame( void ) {
	bool	suggestion = false;

	while (this->run == true) {
		this->render->draw_scene(this->map);
		if (this->_player == 1) {
			suggestion = false;
			if (this->get_move(&this->render->case_event) == true) {
				if (ft42::win_align == true && this->check_if_5_present() == true) {
					this->run = false;
				}
			}
		}
		else {
			if (ft42::two_players == false) {
				gettimeofday(&this->render->_time_start, NULL);
				this->ia->play(this->_player, this->_pos);
				gettimeofday(&this->render->_time_end, NULL);
				if (ft42::win_align == true && this->check_if_5_present() == true) {
					this->run = false;
				}
			}
			else {
				if (suggestion == false) {
					this->ia->play(this->_player, this->_pos);
					suggestion = true;
				}
				if (this->get_move(&this->render->case_event) == true)
					this->ia->checkAlign(&this->_pos);
					if (ft42::win_align == true && this->check_if_5_present() == true) {
					this->run = false;
				}
			}
			
		}
	}
	if (this->gameState.winner != 0) {
		this->run = true;
		this->gameState.statusMessage = e_GAME_END_QUESTION;
		this->render->draw_scene(this->map);
		while (this->run == true)
			this->render->keyboard();
	}
	this->exit_free();
}

void Event::print_map( Map *map ) { // test function
	int x, y = 0;

	while (y < SIZE) {
		x = 0;
		while (x < SIZE) {
			std::cout << (*map)[y][x] << " ";
			x++;
		}
		std::cout << std::endl;
		y++;
	}
}
