#include <main.hpp>

int main( int ac, char **av ) {
	try {
		Event *		event = new Event();

		event->w_log("Event Init");
		event->init(ac, av);
		event->w_log("Event Launch Game");
		event->lauchGame();
		event->w_log("DELETE Event");
		delete event;
	}
	catch (std::exception & e){
		return (EXIT_FAILURE);
	}
	return (EXIT_SUCCESS);
}

t_data *	get_data(void) {
	static t_data *	d = NULL;

	if (NULL == d) {
		d = static_cast<t_data *>( std::malloc( sizeof(t_data) ) );
		if (NULL == d) {
			std::cerr << "Data Initialisation error> try to free memory and try again." << std::endl;
			return NULL;
		}
	}
	return d;
}
