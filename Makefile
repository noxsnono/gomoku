# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/05/29 14:06:15 by vjacquie          #+#    #+#              #
#    Updated: 2015/07/18 16:32:35 by vjacquie         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

PLATFORM := $(shell uname)

#Mac --	brew install sdl2
ifeq "$(PLATFORM)" "Darwin"
	GLUT = -framework GLUT -framework OpenGL -framework Cocoa
	COMMAND = $(CXX) $(CFLAGS) $(SDL_LIB) $(GLUT) $(OBJ) -o $(NAME)
endif
#Linux -- apt-get install libsdl2-dev libsdl2-gfx-dev freeglut3 freeglut3-dev
ifeq "$(PLATFORM)" "Linux"
	GLUT = -lGL -lGLU -lglut
	COMMAND = $(CXX) $(CFLAGS) $(SDL_LIB) $(GLUT) $(OBJ) -o $(NAME)
endif

CXX = 			clang++

NAME =			gomoku

CFLAGS =		-Wall -Werror -Wextra -std=gnu++11 -Wno-deprecated

INCLUDE =		-I includes/

SDL_INCLUDE =	`sdl2-config --cflags`
SDL_LIB =		`sdl2-config --libs`

HEAD = 			includes/Render.class.hpp \
	 			includes/ft42.class.hpp \
	 			includes/event.class.hpp \
	 			includes/calc.class.hpp \
	 			includes/nodes.class.hpp \
	 			includes/ia.class.hpp \
	 			includes/main.hpp \
	 			includes/legend_text.hpp

SRC = 			src/Render.class.cpp \
				src/ft42.class.cpp \
				src/event.class.cpp \
				src/calc.class.cpp\
				src/nodes.class.cpp\
				src/ia.class.cpp\
				src/main.cpp

OBJ = 			$(SRC:.cpp=.cpp.o)

%.cpp.o: %.cpp $(HEAD)
	clear
	@$(CXX) $(CFLAGS)  $(INCLUDE) $(SDL_INCLUDE) $(CFLAGS) -c $< -o $@

all: $(NAME)
	@echo "./gomoku -iathink -log [-2p | -lastmove] -depth=[1-10]"

$(NAME): $(OBJ)
	$(COMMAND)

clean:
	@rm -rf $(OBJ)

fclean: clean
	@rm -rf $(NAME)

re: fclean all

sdltest:
	clear
	$(CXX) $(CFLAGS) -stdlib=libc++ -o testsdl2 test/test_sdl2.cpp \
	-framework opengl `sdl2-config --libs` `sdl2-config --cflags`

.PHONY: all clean fclean re
