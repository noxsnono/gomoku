/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calc.class.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/03 13:29:01 by vjacquie          #+#    #+#             */
/*   Updated: 2015/08/22 14:27:18 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CALC_CLASS_HPP
# define CALC_CLASS_HPP

# include <main.hpp>
# include <ft42.class.hpp>
# include <event.class.hpp>
# include <math.h>

class Event;

class Calc : public ft42 {
public:
	Event *	main_event;
	Map	*	_map;

	Calc( void );
	Calc( Map *map );
	Calc( Calc const & src );
	Calc & operator=( Calc const & rhs );
	virtual ~Calc( void );

	bool	check_win( int player, std::vector<int> pos);
	bool	check_win_hor(int player, std::vector<int> pos);
	bool	check_win_ver(int player, std::vector<int> pos);
	bool	check_win_diag1(int player, std::vector<int> pos);
	bool	check_win_diag2(int player, std::vector<int> pos);
	int		check_capture( int player, std::vector<int> pos );
	bool	check_pos( std::vector<int> pos, int player );
	bool	check_free_three(std::vector<int> pos, int player);
	bool	check_free_three_hor(std::vector<int> pos, int player);
	bool	check_free_three_ver(std::vector<int> pos, int player);
	bool	check_free_three_diag1(std::vector<int> pos, int player);
	bool	check_free_three_diag2(std::vector<int> pos, int player);
	int		check_line( int pos1, int pos2, int player );
	bool	good_val( int x );
	bool	good_val_full( int x, int y );
	int		check_line(std::vector<int> pos, int y, int x, int player );
	int		modif_map(std::vector<int> pos, int y, int x, int player);
	void	init( Event * event, int ac, char **av );
	void	exit_free( void );
	void	clean_list_after_capture( void );
	void	create_new_align( int start[], int end[], int player );
	void	refresh_blocked_align(int y, int x);
	bool	capture_before_win(int player, int start[], int end[]);
	bool	is_capture_possible(int y, int x, int player);
	
protected:

};

#endif