/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nodes.class.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/18 16:24:15 by vjacquie          #+#    #+#             */
/*   Updated: 2015/07/18 18:52:51 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NODES_CLASS_HPP
# define NODES_CLASS_HPP

# include <main.hpp>
# include <ft42.class.hpp>
# include <event.class.hpp>

# define LEAF 1
# define NODES 2

class Event;

class Nodes : public ft42 {
public:
	Nodes( void );
	Nodes( Nodes const & src );
	Nodes & operator=( Nodes const & rhs );
	virtual ~Nodes( void );

	int type;
	bool status;
	std::vector<int>				pos;
	std::list<Nodes *>				*child;
	std::list<Nodes *>::iterator	actuel;
	int		calc_val( void );
	void	next( void );
	Nodes 	get_child( void );
};

#endif