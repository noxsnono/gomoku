#ifndef LEGEND_TEXT_HPP
# define LEGEND_TEXT_HPP

# define SCORE_P1			"Human Points (IA/P2 tokens Captured)"
# define SCORE_P1_Y			50
# define SCORE_P1_VALUE_Y	50

# define SCORE_P2			"IA Points (P1 tokens Captured)"
# define SCORE_P2_PLAYER	"Player (P2 tokens Captured)"
# define SCORE_P2_Y			100

# define IA_TIME_PLAY		"Last IA's time to play (Microsec 1E-06)"
# define IA_TIME_PLAY_Y		150

# define WINNER				"   Win"
# define WINNER_Y			200

# define TO_PLAY			"'s turn"
# define TO_PLAY_Y			300

# define STATUS				"STATUS"
# define STATUS_X			920
# define STATUS_Y			450

// Status message
# define STA_CANT_PLACE		"Cannot put token in this place"

# define STA_IA_CAPTURE		"IA Capture 2 of your tokens"

# define STA_PLAYER_CAPTURE "You Capture 2 token of IA"

# define STA_YOUR_TURN 		"Your Turn"

# define STA_GAME_INIT		"Game initialized"

# define STA_GAME_END_1		"Press Echap to exit"
# define STA_GAME_END_2		"Press XXX to start new game"

# define STA_LINE_1			500
# define STA_LINE_2			550
# define STA_LINE_3			600
# define STA_LINE_4			650

# define COLUMN_LINE		"0    1    2    3    4    5    6    7    8    9   10  11  12  13  14  15  16  17  18"

#endif
