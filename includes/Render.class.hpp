#ifndef RENDER_CLASS_HPP
# define RENDER_CLASS_HPP

# include <SDL.h>
# include <ft42.class.hpp>
# include <event.class.hpp>
# include <legend_text.hpp>
# include <sys/time.h>
# include <time.h>

# ifdef linux
#	include <GL/gl.h>
#	include <GL/glu.h>
#	include <GL/glut.h>
# endif
# ifdef __APPLE__
#	include <openGL/gl.h>
#	include <openGL/glu.h>
#	include <glut/glut.h>
# endif

# define WIDTH 			720
# define HEIGHT 		720
# define LEG_WIDTH		650
# define BLUE_P1		1 // HUMAN
# define RED_P2 		2 // IA
# define LASTPLAYED		3 // Last IA jeton
# define WINNER_P		4
# define SUGGESTION		5
# define CASE_SIZE		20
# define SIZE 			19

class Event;

class Render : public ft42 {
public:
	SDL_Window *	window;
	SDL_Renderer *	renderer;
	SDL_Point *		mouse_position;
	SDL_Event 		event;
	int 			width;
	int 			height;
	int 			case_size;
	int 			ring_size;
	Map *			debugg_map;
	bool			run;
	t_case			case_event;
	Event *			main_event;
	struct timeval	_time_start; // gettimeofday(&this->_time_start, NULL);
	struct timeval	_time_end; // gettimeofday(&this->_time_end, NULL);
	int 			old_ia_pos[2];

	Render();
	virtual ~Render();

	void	init( Event * main_event, int ac, char ** av );
	void	draw_circle(int posX, int posY, int radius, int player);
	void	draw_scene(Map * map);
	void	map_debugg( void );
	void  	keyboard( void );
	void	changeCaseEvent(void);
	void	printString( int x, int y, char *str);
	void	drawLegend( void );
	void	drawStatusMEssage( int legend_x );
	void	leftLegendLine( int x, int y );
	void	draw_iathink();
	void	drawWinnerLine( void );
};

#endif
