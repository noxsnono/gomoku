#ifndef MAIN_HPP
# define MAIN_HPP

# include <cstdlib>
# include <cstring>
# include <exception>
# include <fstream>
# include <iostream>
# include <istream>
# include <list>
// # include <ncurses.h> // seriously ?!
# include <regex>
# include <set>
# include <sstream>
# include <stdexcept>
# include <stdlib.h>
# include <string.h>
# include <string>
# include <time.h>
# include <unistd.h>
# include <vector>

# include <event.class.hpp>
# include <calc.class.hpp>
# include <Render.class.hpp>
# include <nodes.class.hpp>
# include <ia.class.hpp>

class Event;
class Calc;
class Render;

// COMMAND Line Options
static bool	static_log = false; 	// -log
static bool	static_iathink = false; // -iathink

typedef struct s_data { // Store all OBject
	Event		*event;
	// Render *	render;
}t_data;

t_data *	get_data(void);

#endif
