#ifndef EVENT_CLASS_HPP
# define EVENT_CLASS_HPP

# include <main.hpp>
# include <ft42.class.hpp>
# include <calc.class.hpp>
# include <Render.class.hpp>

class 	Calc;
class 	Ia;

class Event : public ft42 {
public:
	int					_player; // 0 or 1
	std::vector<int>	_pos;	// pos[0] = y; pos[1] = x 
	std::vector<int>	_capture;	// capture[player_id] = nbr stone captured
	Render *			render;
	Calc *				calc;
	Ia *				ia;
	Map	*				map;
	bool				run;
	t_gameState			gameState;
	t_line_winner		line_winner;


	Event( void );
	Event( Event const & src );
	Event & operator=( Event const & rhs );
	virtual ~Event( void );

	void	init( int ac, char **av );
	void	exit_free( void );
	void	init_map( void );
	void	lauchGame( void );
	bool	get_move( t_case * coord );
	void	put_stone( std::vector<int> pos );
	void	print_map( Map *map );
	void	parse_command(int ac, char **av);
	bool	check_if_5_present(void);

	int 	get_player();

protected:

};

#endif