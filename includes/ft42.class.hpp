#ifndef FT42_CLASS_HPP
# define FT42_CLASS_HPP

# include <cstdlib>
# include <cstring>
# include <exception>
# include <fstream>
# include <iostream>
# include <istream>
# include <sstream>
# include <string>
# include <vector>
# include <regex>
# include <unistd.h>
# include <stdlib.h>
# include <set>
# include <algorithm>
# include <list>

# define LOG_PATH	"log/debugg.log"

# define SIZE 19

enum STATUS {
	e_GAME_INITIALIZED,
	e_GAME_END_QUESTION,
	e_CAPTURE_IA,
	e_CAPTURE_HUMAN,
	e_YOUR_TURN,
	e_CANT_PLACE
};

enum axis {
	e_HORIZONTAL = 0,
	e_VERTICAL = 1,
	e_DIAGUP = 2, // de gauche a droite la ligne monte
	e_DIAGDOWN = 3 // de gauche a droite la ligne descend
};

enum square {
	e_LEFT_BOTTOM = 1,
	e_BOTTOM = 2,
	e_RIGHT_BOTTOM = 3,
	e_LEFT = 4,
	e_RIGHT = 6,
	e_LEFT_TOP = 7,
	e_TOP = 8,
	e_RIGHT_TOP = 9
};

typedef struct 	s_line_winner {
	int 	p1[2];
	int 	p2[2];
	int 	p3[2];
	int 	p4[2];
	int 	p5[2];
	int 	player;
}				t_line_winner;

typedef struct			s_line {
	int					pStart[2];
	bool				pStart_empty;
	int					pEnd[2];
	bool				pEnd_empty;
	int					player;
	int 				axis;
	int					size;
	bool 				type;
	int 				middle_pos[2];
}						t_line;

typedef struct	s_case {
	int 		x;
	int 		y;
}				t_case;

typedef struct	s_think {
	int 		x;
	int 		y;
	int 		score;
}				t_think;

typedef std::vector< std::vector<int> > Map;

typedef struct s_gameState {
	int	winner; // 0=noWinner 1=HumanWinner 2=IAWinner
	int statusMessage; // enum status
	int predictive; //min max profondeur
	int	tokenIaCaptured;
	int tokenHumanCaptured;
}t_gameState;

class ft42 {
public:
	static bool					logg;
	static bool					iathink;
	static int					entry_d;
	static int					cmd_depth;
	static int					win_i;
	static bool					win_align;
	static bool					draw_last_played;
	static bool					two_players;

	static int 					node_total;
	static int 					score_max;
	static std::list<char *>	node_score;


	ft42();
	virtual ~ft42();

	void					w_log( std::string const newEntry );
	void					w_full( std::string const newEntry );
	void					w_error( std::string const newEntry );
	std::string				logTime( std::string const & sentence );
	char *					serialize(int x, int y, int score);

	std::ofstream &			getLogFD( void );
	Map	*					copy_map( Map *map );

private:
	static std::string		logFileName;
	static std::ofstream	lodFD;

	std::string				result;
	time_t					timer;
	struct tm *				timeinfo;
	char					buffer[80];
};

#endif