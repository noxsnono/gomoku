/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ia.class.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/18 13:53:03 by vjacquie          #+#    #+#             */
/*   Updated: 2015/09/19 15:22:46 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IA_CLASS_HPP
# define IA_CLASS_HPP

# include <main.hpp>
# include <ft42.class.hpp>
# include <event.class.hpp>

# define MAX_DEPTH 10
# define INF 99999999

# define START 	0
# define END 	1

# define CAPTURE 0
# define ALIGN 1

# define HUMAN	1
# define IA 	2

typedef struct								s_node {
	bool									status;
	std::vector<int> *						pos;		// où le pions sera placé
	std::vector<int> *						pos_off;	// dernier coup offensif
	std::vector<int> *						pos_def;	// dernier coup defensif
	std::vector<int> *						pos_adv;	// dernier coup de l'adversaire
	int										player;		// player id used for check
	int 									type;		// node of type off or def
	int 									cost;		// evaluate cost
	int 									node_type;	// min:1 or max:2
	std::list<struct s_node *>				*child;
	std::list<struct s_node *>::iterator	actuel;
	Map *									map;
}											t_node;

class Event;
class Nodes;

class Ia : public ft42 {
	Map *				_map;

public:
	Event *				main_event;
	Calc *				calc;
	std::vector<int>	pos; // ou placer le pions
	std::vector<int> *	pos_off; // dernier coup offensif
	std::vector<int> *	pos_def; // dernier coup defensif
	std::vector<int> *	pos_adv; // dernier coup de l'adversaire
	std::list<t_line *>	line_list;
	int 				suggestion[2];


	Ia( void );
	Ia( Map *map );
	Ia( Ia const & src );
	Ia & operator=( Ia const & rhs );
	virtual ~Ia( void );

	void		init( Event *event, Calc *calc );
	void		play( int player, std::vector<int> pos );
	int			alphabeta(t_node *p, int alpha, int beta );
	void		build_tree( int player, t_node *nodes, int direction, int depth );
	int 		check_around( std::vector<int> pos );
	t_node *	get_new_node( std::vector<int> *off, std::vector<int> *def, std::vector<int> *adv );
	void		next_node( t_node * node );
	t_node * 	get_child_node( t_node * node );
	int			calc_val(t_node *node );
	void		get_around( t_node *node, std::vector<int> *pos, int player, int type );
	void		copy_vect( std::vector<int> *dest, std::vector<int> src );
	void 		get_rand_dir( std::vector<int> *pos, std::vector<int> *move, Map *map );
	void		print_tree( t_node *nodes, int depth ); // test fct
	void 		get_dir( std::vector<int> *pos, std::vector<int> *move, Map *map, int player );
	int 		score_tab( t_node *node );
	void		do_calc_node( t_node * node );
	int			get_line_size(std::vector<int> pos, int y, int x, int player, Map *map);
	void		checkAlign( std::vector<int> *pos );
	int 		pos_compare(std::vector<int> *pos1, int pos2[2]);
	void		move_point_chain( t_line * line , std::vector<int> *pos, int axis, int type);
	t_line *	t_lineAlloc( void );
	void		addSingleNode(std::vector<int> pos, t_node *node, int player);
	void		set_node_status( t_node * node );
	void 		play_rand( void );
	int 		get_score(int size, int type, int player);
	int			eval_capture(std::vector<int> pos, int y, int x, int player);
	void		check_spaced_align(std::vector<int> *pos);
	void		add_spaced_align(int y, int x, int player, int axis, int dir);
	int 		eval_spaced_align(std::vector<int> pos);
	void		play_where_capture( void );
	void    	free_nodes(t_node *toDelete);

};

bool is_remove(const t_line *line);
#endif
